﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mCB1F3EC182E4244375AA5860196B7174464296EB (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m9022BF8F3F70511C759CB64F33D936A09C568E82 (void);
// 0x00000003 System.Void System.ThrowHelper::ThrowArgumentNullException(System.ExceptionArgument)
extern void ThrowHelper_ThrowArgumentNullException_m1A2C8635322C8C90DCADB318972117265977607F (void);
// 0x00000004 System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException(System.ExceptionArgument)
extern void ThrowHelper_ThrowArgumentOutOfRangeException_mA57669F3390C3DD900C8F12AF50A5E0824FA8A6E (void);
// 0x00000005 System.ArgumentNullException System.ThrowHelper::GetArgumentNullException(System.ExceptionArgument)
extern void ThrowHelper_GetArgumentNullException_m7779AFA514CDCC7096F8FE30ED5298B00CA9993B (void);
// 0x00000006 System.ArgumentOutOfRangeException System.ThrowHelper::GetArgumentOutOfRangeException(System.ExceptionArgument)
extern void ThrowHelper_GetArgumentOutOfRangeException_m5A0B2F358C6F70C235B60E120A9DC306D4EC7B52 (void);
// 0x00000007 System.String System.ThrowHelper::GetArgumentName(System.ExceptionArgument)
extern void ThrowHelper_GetArgumentName_mA83DF484BE63BB7A1B37E3BEDB859F55061CA5EE (void);
// 0x00000008 System.Threading.Tasks.Task System.Threading.Tasks.ValueTask::get_CompletedTask()
extern void ValueTask_get_CompletedTask_m479F83B6B0C1AF8D43BBB1AE35C683BDC5D40032 (void);
// 0x00000009 System.Int32 System.Threading.Tasks.ValueTask::GetHashCode()
extern void ValueTask_GetHashCode_m158970B1DD3DAF5234C01F4432725726CBBE220B (void);
// 0x0000000A System.Boolean System.Threading.Tasks.ValueTask::Equals(System.Object)
extern void ValueTask_Equals_m7A88C17E06E7B9CA1414C8CB8803951E7144ACC9 (void);
// 0x0000000B System.Boolean System.Threading.Tasks.ValueTask::Equals(System.Threading.Tasks.ValueTask)
extern void ValueTask_Equals_m4BF54849B66845D4746F32108CA53127D15D9DB0 (void);
// 0x0000000C System.Void System.Threading.Tasks.ValueTask::.cctor()
extern void ValueTask__cctor_m8203D595DB8C4C36371358F887AD6682F3BD6293 (void);
// 0x0000000D System.Void System.Threading.Tasks.ValueTask`1::.ctor(TResult)
// 0x0000000E System.Void System.Threading.Tasks.ValueTask`1::.ctor(System.Threading.Tasks.Task`1<TResult>)
// 0x0000000F System.Void System.Threading.Tasks.ValueTask`1::.ctor(System.Threading.Tasks.Sources.IValueTaskSource`1<TResult>,System.Int16)
// 0x00000010 System.Int32 System.Threading.Tasks.ValueTask`1::GetHashCode()
// 0x00000011 System.Boolean System.Threading.Tasks.ValueTask`1::Equals(System.Object)
// 0x00000012 System.Boolean System.Threading.Tasks.ValueTask`1::Equals(System.Threading.Tasks.ValueTask`1<TResult>)
// 0x00000013 System.Boolean System.Threading.Tasks.ValueTask`1::get_IsCompleted()
// 0x00000014 System.Boolean System.Threading.Tasks.ValueTask`1::get_IsCompletedSuccessfully()
// 0x00000015 TResult System.Threading.Tasks.ValueTask`1::get_Result()
// 0x00000016 System.Runtime.CompilerServices.ValueTaskAwaiter`1<TResult> System.Threading.Tasks.ValueTask`1::GetAwaiter()
// 0x00000017 System.String System.Threading.Tasks.ValueTask`1::ToString()
// 0x00000018 System.Void System.Threading.Tasks.Sources.IValueTaskSource::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16,System.Threading.Tasks.Sources.ValueTaskSourceOnCompletedFlags)
// 0x00000019 System.Threading.Tasks.Sources.ValueTaskSourceStatus System.Threading.Tasks.Sources.IValueTaskSource`1::GetStatus(System.Int16)
// 0x0000001A System.Void System.Threading.Tasks.Sources.IValueTaskSource`1::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16,System.Threading.Tasks.Sources.ValueTaskSourceOnCompletedFlags)
// 0x0000001B TResult System.Threading.Tasks.Sources.IValueTaskSource`1::GetResult(System.Int16)
// 0x0000001C System.Void System.Runtime.CompilerServices.AsyncMethodBuilderAttribute::.ctor(System.Type)
extern void AsyncMethodBuilderAttribute__ctor_m4E3C84FE67413BD5777E5944C811C904D7DED2CB (void);
// 0x0000001D System.Void System.Runtime.CompilerServices.ValueTaskAwaiter::OnCompleted(System.Action)
extern void ValueTaskAwaiter_OnCompleted_m1AF27A1417DFCA011318067EB4CCC7CCC4228C50 (void);
// 0x0000001E System.Void System.Runtime.CompilerServices.ValueTaskAwaiter::UnsafeOnCompleted(System.Action)
extern void ValueTaskAwaiter_UnsafeOnCompleted_mEDC1D020D201BA364B538D370549F8A4F22960C8 (void);
// 0x0000001F System.Void System.Runtime.CompilerServices.ValueTaskAwaiter::.cctor()
extern void ValueTaskAwaiter__cctor_m3490315142834A70A583D6E3CED4FBAE06EE054E (void);
// 0x00000020 System.Void System.Runtime.CompilerServices.ValueTaskAwaiter/<>c::.cctor()
extern void U3CU3Ec__cctor_m0CE782D27FD1D658DEF13B757035131BFC2BCD40 (void);
// 0x00000021 System.Void System.Runtime.CompilerServices.ValueTaskAwaiter/<>c::.ctor()
extern void U3CU3Ec__ctor_m97FE77D6EFC19182BEB6495E43E203724B8BE4F2 (void);
// 0x00000022 System.Void System.Runtime.CompilerServices.ValueTaskAwaiter/<>c::<.cctor>b__9_0(System.Object)
extern void U3CU3Ec_U3C_cctorU3Eb__9_0_m892CC118EE44F242E4934434CF45123AEB902316 (void);
// 0x00000023 System.Void System.Runtime.CompilerServices.ValueTaskAwaiter`1::.ctor(System.Threading.Tasks.ValueTask`1<TResult>)
// 0x00000024 System.Boolean System.Runtime.CompilerServices.ValueTaskAwaiter`1::get_IsCompleted()
// 0x00000025 TResult System.Runtime.CompilerServices.ValueTaskAwaiter`1::GetResult()
// 0x00000026 System.Void System.Runtime.CompilerServices.ValueTaskAwaiter`1::OnCompleted(System.Action)
// 0x00000027 System.Void System.Runtime.CompilerServices.ValueTaskAwaiter`1::UnsafeOnCompleted(System.Action)
// 0x00000028 System.Void System.Diagnostics.StackTraceHiddenAttribute::.ctor()
extern void StackTraceHiddenAttribute__ctor_m044B630864949DDCBF18FAC1D702A8ADF09F5F17 (void);
static Il2CppMethodPointer s_methodPointers[40] = 
{
	EmbeddedAttribute__ctor_mCB1F3EC182E4244375AA5860196B7174464296EB,
	IsReadOnlyAttribute__ctor_m9022BF8F3F70511C759CB64F33D936A09C568E82,
	ThrowHelper_ThrowArgumentNullException_m1A2C8635322C8C90DCADB318972117265977607F,
	ThrowHelper_ThrowArgumentOutOfRangeException_mA57669F3390C3DD900C8F12AF50A5E0824FA8A6E,
	ThrowHelper_GetArgumentNullException_m7779AFA514CDCC7096F8FE30ED5298B00CA9993B,
	ThrowHelper_GetArgumentOutOfRangeException_m5A0B2F358C6F70C235B60E120A9DC306D4EC7B52,
	ThrowHelper_GetArgumentName_mA83DF484BE63BB7A1B37E3BEDB859F55061CA5EE,
	ValueTask_get_CompletedTask_m479F83B6B0C1AF8D43BBB1AE35C683BDC5D40032,
	ValueTask_GetHashCode_m158970B1DD3DAF5234C01F4432725726CBBE220B,
	ValueTask_Equals_m7A88C17E06E7B9CA1414C8CB8803951E7144ACC9,
	ValueTask_Equals_m4BF54849B66845D4746F32108CA53127D15D9DB0,
	ValueTask__cctor_m8203D595DB8C4C36371358F887AD6682F3BD6293,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AsyncMethodBuilderAttribute__ctor_m4E3C84FE67413BD5777E5944C811C904D7DED2CB,
	ValueTaskAwaiter_OnCompleted_m1AF27A1417DFCA011318067EB4CCC7CCC4228C50,
	ValueTaskAwaiter_UnsafeOnCompleted_mEDC1D020D201BA364B538D370549F8A4F22960C8,
	ValueTaskAwaiter__cctor_m3490315142834A70A583D6E3CED4FBAE06EE054E,
	U3CU3Ec__cctor_m0CE782D27FD1D658DEF13B757035131BFC2BCD40,
	U3CU3Ec__ctor_m97FE77D6EFC19182BEB6495E43E203724B8BE4F2,
	U3CU3Ec_U3C_cctorU3Eb__9_0_m892CC118EE44F242E4934434CF45123AEB902316,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	StackTraceHiddenAttribute__ctor_m044B630864949DDCBF18FAC1D702A8ADF09F5F17,
};
extern void ValueTask_GetHashCode_m158970B1DD3DAF5234C01F4432725726CBBE220B_AdjustorThunk (void);
extern void ValueTask_Equals_m7A88C17E06E7B9CA1414C8CB8803951E7144ACC9_AdjustorThunk (void);
extern void ValueTask_Equals_m4BF54849B66845D4746F32108CA53127D15D9DB0_AdjustorThunk (void);
extern void ValueTaskAwaiter_OnCompleted_m1AF27A1417DFCA011318067EB4CCC7CCC4228C50_AdjustorThunk (void);
extern void ValueTaskAwaiter_UnsafeOnCompleted_mEDC1D020D201BA364B538D370549F8A4F22960C8_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[5] = 
{
	{ 0x06000009, ValueTask_GetHashCode_m158970B1DD3DAF5234C01F4432725726CBBE220B_AdjustorThunk },
	{ 0x0600000A, ValueTask_Equals_m7A88C17E06E7B9CA1414C8CB8803951E7144ACC9_AdjustorThunk },
	{ 0x0600000B, ValueTask_Equals_m4BF54849B66845D4746F32108CA53127D15D9DB0_AdjustorThunk },
	{ 0x0600001D, ValueTaskAwaiter_OnCompleted_m1AF27A1417DFCA011318067EB4CCC7CCC4228C50_AdjustorThunk },
	{ 0x0600001E, ValueTaskAwaiter_UnsafeOnCompleted_mEDC1D020D201BA364B538D370549F8A4F22960C8_AdjustorThunk },
};
static const int32_t s_InvokerIndices[40] = 
{
	6265,
	6265,
	9069,
	9069,
	8788,
	8788,
	8788,
	9189,
	6112,
	3659,
	3748,
	9224,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1177,
	-1,
	-1,
	-1,
	5130,
	5130,
	5130,
	9224,
	9224,
	6265,
	5130,
	-1,
	-1,
	-1,
	-1,
	-1,
	6265,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000007, { 0, 15 } },
	{ 0x02000011, { 15, 8 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[23] = 
{
	{ (Il2CppRGCTXDataType)2, 1120 },
	{ (Il2CppRGCTXDataType)2, 14201 },
	{ (Il2CppRGCTXDataType)3, 55150 },
	{ (Il2CppRGCTXDataType)3, 19031 },
	{ (Il2CppRGCTXDataType)2, 5570 },
	{ (Il2CppRGCTXDataType)3, 19030 },
	{ (Il2CppRGCTXDataType)2, 13497 },
	{ (Il2CppRGCTXDataType)3, 63756 },
	{ (Il2CppRGCTXDataType)2, 9398 },
	{ (Il2CppRGCTXDataType)3, 51793 },
	{ (Il2CppRGCTXDataType)3, 51052 },
	{ (Il2CppRGCTXDataType)2, 14195 },
	{ (Il2CppRGCTXDataType)3, 55131 },
	{ (Il2CppRGCTXDataType)3, 55151 },
	{ (Il2CppRGCTXDataType)3, 55152 },
	{ (Il2CppRGCTXDataType)3, 55148 },
	{ (Il2CppRGCTXDataType)3, 55149 },
	{ (Il2CppRGCTXDataType)2, 13496 },
	{ (Il2CppRGCTXDataType)3, 51792 },
	{ (Il2CppRGCTXDataType)3, 51050 },
	{ (Il2CppRGCTXDataType)3, 63755 },
	{ (Il2CppRGCTXDataType)2, 9397 },
	{ (Il2CppRGCTXDataType)3, 51051 },
};
extern const CustomAttributesCacheGenerator g_System_Threading_Tasks_Extensions_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Threading_Tasks_Extensions_CodeGenModule;
const Il2CppCodeGenModule g_System_Threading_Tasks_Extensions_CodeGenModule = 
{
	"System.Threading.Tasks.Extensions.dll",
	40,
	s_methodPointers,
	5,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	23,
	s_rgctxValues,
	NULL,
	g_System_Threading_Tasks_Extensions_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
