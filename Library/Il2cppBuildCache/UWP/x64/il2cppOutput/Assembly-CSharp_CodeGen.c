﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void RosSharp.TransformExtensions::DestroyImmediateIfExists(UnityEngine.Transform)
// 0x00000002 T RosSharp.TransformExtensions::AddComponentIfNotExists(UnityEngine.Transform)
// 0x00000003 System.Void RosSharp.TransformExtensions::SetParentAndAlign(UnityEngine.Transform,UnityEngine.Transform,System.Boolean)
extern void TransformExtensions_SetParentAndAlign_m3777C3513290D7C618F369C34EECDB1750BB1AE0 (void);
// 0x00000004 System.Boolean RosSharp.TransformExtensions::HasExactlyOneChild(UnityEngine.Transform)
extern void TransformExtensions_HasExactlyOneChild_m17FDA8B3ABC5E2F943CACB9D8BF6E82772AE1846 (void);
// 0x00000005 System.Void RosSharp.TransformExtensions::MoveChildTransformToParent(UnityEngine.Transform,System.Boolean)
extern void TransformExtensions_MoveChildTransformToParent_m281BF82F78DF9DEDF57E6C1D800BC065049E5230 (void);
// 0x00000006 UnityEngine.Vector3 RosSharp.TransformExtensions::Ros2Unity(UnityEngine.Vector3)
extern void TransformExtensions_Ros2Unity_m11467E18A90D469732140E3C3EB11904393C06FE (void);
// 0x00000007 UnityEngine.Vector3 RosSharp.TransformExtensions::Unity2Ros(UnityEngine.Vector3)
extern void TransformExtensions_Unity2Ros_m74703B5E4DEFB516AB071300101C988D494E52E1 (void);
// 0x00000008 UnityEngine.Vector3 RosSharp.TransformExtensions::Ros2UnityScale(UnityEngine.Vector3)
extern void TransformExtensions_Ros2UnityScale_m33150967D5F4DA5AE28E1F33E2792DC71460B3CA (void);
// 0x00000009 UnityEngine.Vector3 RosSharp.TransformExtensions::Unity2RosScale(UnityEngine.Vector3)
extern void TransformExtensions_Unity2RosScale_m2C3C5965D35797490D2D77DD8E3DDC325166D55E (void);
// 0x0000000A UnityEngine.Quaternion RosSharp.TransformExtensions::Ros2Unity(UnityEngine.Quaternion)
extern void TransformExtensions_Ros2Unity_m9A91003341D53EAEE8EC18EFE3353F213E717728 (void);
// 0x0000000B UnityEngine.Quaternion RosSharp.TransformExtensions::Unity2Ros(UnityEngine.Quaternion)
extern void TransformExtensions_Unity2Ros_m0A420C0A2D4862981403680087FC5CB19E90558D (void);
// 0x0000000C System.Double[] RosSharp.TransformExtensions::ToRoundedDoubleArray(UnityEngine.Vector3)
extern void TransformExtensions_ToRoundedDoubleArray_m6C4AD90B183308C3756479E95F2C5A061F043C6A (void);
// 0x0000000D UnityEngine.Vector3 RosSharp.TransformExtensions::ToVector3(System.Double[])
extern void TransformExtensions_ToVector3_mFA2848545680A5B674DEBCDF26AF41114E838672 (void);
// 0x0000000E System.String RosSharp.TransformExtensions::SetSeparatorChar(System.String)
extern void TransformExtensions_SetSeparatorChar_mC6095D5815B819F17C287ADB6BD16EFCDE3E1498 (void);
// 0x0000000F System.Single RosSharp.HingeJointLimitsManager::get_AngleActual()
extern void HingeJointLimitsManager_get_AngleActual_m78E09DBCC192148981559333426CEBFCA80C397D (void);
// 0x00000010 System.Void RosSharp.HingeJointLimitsManager::set_AngleActual(System.Single)
extern void HingeJointLimitsManager_set_AngleActual_m9BB4978C9FF2C6058CABFC4462E2B0DC0F57F2C8 (void);
// 0x00000011 System.Int32 RosSharp.HingeJointLimitsManager::get_RotationNumberActual()
extern void HingeJointLimitsManager_get_RotationNumberActual_m7B068097B0F522D9F03F1A9D850B3B76FC7150C6 (void);
// 0x00000012 System.Void RosSharp.HingeJointLimitsManager::set_RotationNumberActual(System.Int32)
extern void HingeJointLimitsManager_set_RotationNumberActual_m3F41CEE79E28D2C53CB6CA7BC523AE0F3AF472FF (void);
// 0x00000013 System.Int32 RosSharp.HingeJointLimitsManager::get_RotationNumberMin()
extern void HingeJointLimitsManager_get_RotationNumberMin_m81F5417EE86CD5F570B33CD8CF003CE1F2157C7F (void);
// 0x00000014 System.Void RosSharp.HingeJointLimitsManager::set_RotationNumberMin(System.Int32)
extern void HingeJointLimitsManager_set_RotationNumberMin_mE5F456566B3A01F4C5F2B223623C415A4388C204 (void);
// 0x00000015 System.Int32 RosSharp.HingeJointLimitsManager::get_RotationNumberMax()
extern void HingeJointLimitsManager_get_RotationNumberMax_m293FB24FD097B90CC9F115997409F3AAA1AB56EA (void);
// 0x00000016 System.Void RosSharp.HingeJointLimitsManager::set_RotationNumberMax(System.Int32)
extern void HingeJointLimitsManager_set_RotationNumberMax_mB7BC17C415248A9CBC19C270EEE5F3CB323EB0D5 (void);
// 0x00000017 System.Single RosSharp.HingeJointLimitsManager::get_AngleLimitMin()
extern void HingeJointLimitsManager_get_AngleLimitMin_m3764FD62F95C4FEE3C779F783CACFD2AB21ADD5F (void);
// 0x00000018 System.Void RosSharp.HingeJointLimitsManager::set_AngleLimitMin(System.Single)
extern void HingeJointLimitsManager_set_AngleLimitMin_mB88421F9F14F2BA476E7D5F0FA5AC1B2F19AAC6C (void);
// 0x00000019 System.Single RosSharp.HingeJointLimitsManager::get_AngleLimitMax()
extern void HingeJointLimitsManager_get_AngleLimitMax_m8CB0D26BF2922509731C4E7403730A0B9C678A02 (void);
// 0x0000001A System.Void RosSharp.HingeJointLimitsManager::set_AngleLimitMax(System.Single)
extern void HingeJointLimitsManager_set_AngleLimitMax_m7AD295FD06810FF2727B882182974E476D7F79A2 (void);
// 0x0000001B System.Void RosSharp.HingeJointLimitsManager::Awake()
extern void HingeJointLimitsManager_Awake_m081555A1647E57CC3C89A413CDB265E98C484163 (void);
// 0x0000001C System.Void RosSharp.HingeJointLimitsManager::Update()
extern void HingeJointLimitsManager_Update_mAF5284F59611F61DC202C9CA459617A085A1F7E2 (void);
// 0x0000001D System.Void RosSharp.HingeJointLimitsManager::FixedUpdate()
extern void HingeJointLimitsManager_FixedUpdate_m9B14D977F51D3AD9ACED9F374C3A91FA1B3F2B4A (void);
// 0x0000001E System.Void RosSharp.HingeJointLimitsManager::RecalculateJointLimits()
extern void HingeJointLimitsManager_RecalculateJointLimits_mA2B534DCA05835B60CCD76235EABDCA9A040D91C (void);
// 0x0000001F System.Void RosSharp.HingeJointLimitsManager::FixAngleLimits()
extern void HingeJointLimitsManager_FixAngleLimits_mF2DF41ED7052D6FC223114726292A4A3D5F6C8BB (void);
// 0x00000020 System.Single RosSharp.HingeJointLimitsManager::GetAngleLimit(System.Single)
extern void HingeJointLimitsManager_GetAngleLimit_m5DC0FD456E0AE5EEC707C8865580AFA936CF5C07 (void);
// 0x00000021 System.Int32 RosSharp.HingeJointLimitsManager::GetRotationLimit(System.Single)
extern void HingeJointLimitsManager_GetRotationLimit_m8437873DBDEC77BC9FF99C4C7C1C7D0363B68B74 (void);
// 0x00000022 System.Void RosSharp.HingeJointLimitsManager::UpdateAngles()
extern void HingeJointLimitsManager_UpdateAngles_mCC71C15933C9797767519FE1CA7902F0FC9A6F26 (void);
// 0x00000023 System.Void RosSharp.HingeJointLimitsManager::ApplyJointLimits()
extern void HingeJointLimitsManager_ApplyJointLimits_m0E7DC9996E308CCB09EED2E0B279E69637124DBF (void);
// 0x00000024 UnityEngine.JointLimits RosSharp.HingeJointLimitsManager::UpdateLimits(UnityEngine.JointLimits,System.Single,System.Single)
extern void HingeJointLimitsManager_UpdateLimits_mAA5708CA0843DB903C11D1155D8A29F34DD30720 (void);
// 0x00000025 System.Void RosSharp.HingeJointLimitsManager::InitializeLimits(RosSharp.Urdf.Joint/Limit,UnityEngine.HingeJoint)
extern void HingeJointLimitsManager_InitializeLimits_m17A712C8BBC2274E58ED930366F9101E27F680D3 (void);
// 0x00000026 System.Void RosSharp.HingeJointLimitsManager::.ctor()
extern void HingeJointLimitsManager__ctor_m0B100866C1C5167DDF2AD8E8D734DBB9CDFF1C16 (void);
// 0x00000027 System.Void RosSharp.PrismaticJointLimitsManager::Awake()
extern void PrismaticJointLimitsManager_Awake_m0D2113332557B71292D59090A63478D09F40DE34 (void);
// 0x00000028 System.Void RosSharp.PrismaticJointLimitsManager::FixedUpdate()
extern void PrismaticJointLimitsManager_FixedUpdate_m1ED0DC0653ED7B4B6D328D29901925A8ED3C43E9 (void);
// 0x00000029 System.Void RosSharp.PrismaticJointLimitsManager::OnValidate()
extern void PrismaticJointLimitsManager_OnValidate_mF69C06A13B2D2105C3C164226DCF5098981A4329 (void);
// 0x0000002A System.Void RosSharp.PrismaticJointLimitsManager::ApplyLimits()
extern void PrismaticJointLimitsManager_ApplyLimits_m71EE903FA157F2041B34D8865744E79BAE6CA1BA (void);
// 0x0000002B UnityEngine.SoftJointLimit RosSharp.PrismaticJointLimitsManager::UpdateLimit(UnityEngine.SoftJointLimit,System.Single)
extern void PrismaticJointLimitsManager_UpdateLimit_m6DD4ADEFD225A1D5F0FC6B3347C54A56AC9B3966 (void);
// 0x0000002C System.Void RosSharp.PrismaticJointLimitsManager::InitializeLimits(RosSharp.Urdf.Joint/Limit)
extern void PrismaticJointLimitsManager_InitializeLimits_mFBC54B956CB1B66C8C68FEB61E620B72EDC9C529 (void);
// 0x0000002D System.Void RosSharp.PrismaticJointLimitsManager::.ctor()
extern void PrismaticJointLimitsManager__ctor_mD4C7C18133BAF3B24BDF43F69540359FDFDA48EE (void);
// 0x0000002E System.Void RosSharp.Matrix3x3::.ctor(System.Single)
extern void Matrix3x3__ctor_mE0DE3B45AB53FDA2326CFE93F6BAD79346763C08 (void);
// 0x0000002F System.Void RosSharp.Matrix3x3::.ctor(System.Single[])
extern void Matrix3x3__ctor_m6B9C729CF734F0C21CAD9FF1152748F33332019B (void);
// 0x00000030 System.Void RosSharp.Matrix3x3::.ctor(System.Single[][])
extern void Matrix3x3__ctor_m27F9291C3D3C0A68F8A017AB16CDE75ED600FFD0 (void);
// 0x00000031 System.Void RosSharp.Matrix3x3::.ctor(UnityEngine.Vector3[])
extern void Matrix3x3__ctor_mF8613E46AC17A5DBE4D4BF9472EB232E85A5BFAB (void);
// 0x00000032 System.Single[] RosSharp.Matrix3x3::get_Item(System.Int32)
extern void Matrix3x3_get_Item_m610B8C72B0C88596013D1A2912FD8E805EC7A96D (void);
// 0x00000033 System.Void RosSharp.Matrix3x3::set_Item(System.Int32,System.Single[])
extern void Matrix3x3_set_Item_m8A1441B7DCC830641573E07E5043C973F8583F22 (void);
// 0x00000034 RosSharp.Matrix3x3 RosSharp.Matrix3x3::op_Addition(RosSharp.Matrix3x3,RosSharp.Matrix3x3)
extern void Matrix3x3_op_Addition_m8DFB580605C0011CC3DA7D8480C62B3AA7A884DE (void);
// 0x00000035 RosSharp.Matrix3x3 RosSharp.Matrix3x3::op_Addition(RosSharp.Matrix3x3,System.Single)
extern void Matrix3x3_op_Addition_mE5AB6CF747CCB4F38AE179BC10033DDBCF3AC9D1 (void);
// 0x00000036 RosSharp.Matrix3x3 RosSharp.Matrix3x3::op_Subtraction(RosSharp.Matrix3x3,RosSharp.Matrix3x3)
extern void Matrix3x3_op_Subtraction_mE9656C50B713E16317F3AD3A0C7D0FE32B272833 (void);
// 0x00000037 RosSharp.Matrix3x3 RosSharp.Matrix3x3::op_Subtraction(RosSharp.Matrix3x3,System.Single)
extern void Matrix3x3_op_Subtraction_m9B055CD0B0F3F0B56648581DDF1D2175E7247DE1 (void);
// 0x00000038 RosSharp.Matrix3x3 RosSharp.Matrix3x3::op_Multiply(RosSharp.Matrix3x3,System.Single)
extern void Matrix3x3_op_Multiply_m0DE9707A47A82FCE110704BE18AEE3480EC755B1 (void);
// 0x00000039 RosSharp.Matrix3x3 RosSharp.Matrix3x3::op_Multiply(RosSharp.Matrix3x3,RosSharp.Matrix3x3)
extern void Matrix3x3_op_Multiply_mF9C33D4AB925BEB9B7A0BC60DACAF205E901646D (void);
// 0x0000003A UnityEngine.Vector3 RosSharp.Matrix3x3::op_Multiply(RosSharp.Matrix3x3,UnityEngine.Vector3)
extern void Matrix3x3_op_Multiply_mF7A82855527329FACE92509EFEE6AE14642C9C49 (void);
// 0x0000003B RosSharp.Matrix3x3 RosSharp.Matrix3x3::op_Multiply(UnityEngine.Vector3,RosSharp.Matrix3x3)
extern void Matrix3x3_op_Multiply_m3C05F5EAD02795A7D215696C8D28BAA0A436223B (void);
// 0x0000003C RosSharp.Matrix3x3 RosSharp.Matrix3x3::VectorMult(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Matrix3x3_VectorMult_mFFCEBB2C3875E892653A25729143A58690BAA4E9 (void);
// 0x0000003D System.Single RosSharp.Matrix3x3::Determinant()
extern void Matrix3x3_Determinant_m7BD915468295A8E1AAF6800D3FB8CD4565396C7D (void);
// 0x0000003E System.Single RosSharp.Matrix3x3::Trace()
extern void Matrix3x3_Trace_mD8CEEBAB1F09366D867FBC679B568E727ED7B70B (void);
// 0x0000003F System.Boolean RosSharp.Matrix3x3::IsDiagonal()
extern void Matrix3x3_IsDiagonal_mBD4AEC38A96F1BA83FE6F13C1A27DD520B515386 (void);
// 0x00000040 RosSharp.Matrix3x3 RosSharp.Matrix3x3::Transpose()
extern void Matrix3x3_Transpose_m8B922F09ADC03C719BBC879D090F72DBDBCE0E17 (void);
// 0x00000041 System.Void RosSharp.Matrix3x3::DiagonalizeRealSymmetric(UnityEngine.Vector3&,UnityEngine.Vector3[]&)
extern void Matrix3x3_DiagonalizeRealSymmetric_m61171FCB0F832068D9C97BD0DD8C2FFD65BFC499 (void);
// 0x00000042 UnityEngine.Vector3 RosSharp.Matrix3x3::Eigenvalues()
extern void Matrix3x3_Eigenvalues_m51F3BE4FC3B8C06BD760AF8F7232677F089ACB60 (void);
// 0x00000043 UnityEngine.Vector3[] RosSharp.Matrix3x3::Eigenvectors(System.Single[])
extern void Matrix3x3_Eigenvectors_m2DA9DAE008CDED046767058D03479323A8D65B93 (void);
// 0x00000044 UnityEngine.Vector3 RosSharp.Matrix3x3::GetEigenvector0(System.Single[])
extern void Matrix3x3_GetEigenvector0_m269C859F31F2F3146488E959AE25940A9CE25308 (void);
// 0x00000045 UnityEngine.Vector3 RosSharp.Matrix3x3::GetEigenvector1(System.Single[],UnityEngine.Vector3)
extern void Matrix3x3_GetEigenvector1_m3EEC60FC4C4ADB06527BAF1B23449D820D518A57 (void);
// 0x00000046 UnityEngine.Vector3 RosSharp.Matrix3x3::GetEigenvector2(System.Single[],UnityEngine.Vector3,UnityEngine.Vector3)
extern void Matrix3x3_GetEigenvector2_mB33265DB21B6EAD34C8526097A5A7C74306E880C (void);
// 0x00000047 UnityEngine.Vector3[] RosSharp.Matrix3x3::CalculateOrthogonalComplement(UnityEngine.Vector3)
extern void Matrix3x3_CalculateOrthogonalComplement_m83AD44186B0AB721FF34C824B0A7FD1E4CF9F3F0 (void);
// 0x00000048 System.Boolean RosSharp.Matrix3x3::IsTwoEigenvaluesEqual(System.Single[])
extern void Matrix3x3_IsTwoEigenvaluesEqual_m806ED5AD5B0423D4E8911D9E5D4A77C9064031B2 (void);
// 0x00000049 System.Void RosSharp.Urdf.UrdfCollision::.ctor()
extern void UrdfCollision__ctor_m7793B962C349230D061ACCEC029BF4CE397DBBBD (void);
// 0x0000004A System.Void RosSharp.Urdf.UrdfCollisions::.ctor()
extern void UrdfCollisions__ctor_m53AAD614EA26097FCD16C559F23B7B13875A647D (void);
// 0x0000004B System.Void RosSharp.Urdf.UrdfInertial::Create(UnityEngine.GameObject,RosSharp.Urdf.Link/Inertial)
extern void UrdfInertial_Create_mD9FF1702FC81F395C3424B1DF27081E0E3D96DAE (void);
// 0x0000004C System.Void RosSharp.Urdf.UrdfInertial::Initialize()
extern void UrdfInertial_Initialize_mB2916A97B02CB3997BA74AAA95E27CFBE1261C2E (void);
// 0x0000004D System.Void RosSharp.Urdf.UrdfInertial::Reset()
extern void UrdfInertial_Reset_m25EA58756731DD6740CC880901C136DC5E7C065A (void);
// 0x0000004E System.Void RosSharp.Urdf.UrdfInertial::OnValidate()
extern void UrdfInertial_OnValidate_mBBD257430D2FE8FDA8F428EF71AECCE0013100DB (void);
// 0x0000004F System.Void RosSharp.Urdf.UrdfInertial::UpdateRigidBodyData()
extern void UrdfInertial_UpdateRigidBodyData_m6A5BD6A7186551CC741C18B8DADA83D26C8FEB7A (void);
// 0x00000050 System.Void RosSharp.Urdf.UrdfInertial::OnDrawGizmosSelected()
extern void UrdfInertial_OnDrawGizmosSelected_m254F8AFE534D67EE1B3B22BE1AAF423E5E2264B0 (void);
// 0x00000051 System.Void RosSharp.Urdf.UrdfInertial::ImportInertiaData(RosSharp.Urdf.Link/Inertial/Inertia)
extern void UrdfInertial_ImportInertiaData_mFF7DFCD0E7C8D3CADEC01B173CEBB0BFE903A71F (void);
// 0x00000052 UnityEngine.Vector3 RosSharp.Urdf.UrdfInertial::ToUnityInertiaTensor(UnityEngine.Vector3)
extern void UrdfInertial_ToUnityInertiaTensor_mF73ED748D0F4FE156FAABCDA9C072625161EB587 (void);
// 0x00000053 RosSharp.Matrix3x3 RosSharp.Urdf.UrdfInertial::ToMatrix3x3(RosSharp.Urdf.Link/Inertial/Inertia)
extern void UrdfInertial_ToMatrix3x3_mD43846549283D076FBFDC60E28D89391284C9014 (void);
// 0x00000054 UnityEngine.Vector3 RosSharp.Urdf.UrdfInertial::FixMinInertia(UnityEngine.Vector3)
extern void UrdfInertial_FixMinInertia_mF5EF4ED228EECFDA7D4381BACF50C4A6CB3D31F7 (void);
// 0x00000055 UnityEngine.Quaternion RosSharp.Urdf.UrdfInertial::ToQuaternion(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void UrdfInertial_ToQuaternion_m5D5500ABC7A74024F0D424BF44F500BDEF197EDE (void);
// 0x00000056 RosSharp.Urdf.Link/Inertial RosSharp.Urdf.UrdfInertial::ExportInertialData()
extern void UrdfInertial_ExportInertialData_m89A5380F51A33887CBCD7AB73E77A40396DDDA2E (void);
// 0x00000057 RosSharp.Urdf.Link/Inertial/Inertia RosSharp.Urdf.UrdfInertial::ExportInertiaData(UnityEngine.Rigidbody)
extern void UrdfInertial_ExportInertiaData_m81A547844C2824B8F55B072B348A0C7B75FE187C (void);
// 0x00000058 RosSharp.Matrix3x3 RosSharp.Urdf.UrdfInertial::Quaternion2Matrix(UnityEngine.Quaternion)
extern void UrdfInertial_Quaternion2Matrix_mD487E28D9A95B7762BF1DE22876C439BBBCFBB79 (void);
// 0x00000059 RosSharp.Urdf.Link/Inertial/Inertia RosSharp.Urdf.UrdfInertial::ToInertia(RosSharp.Matrix3x3)
extern void UrdfInertial_ToInertia_mE6BA124DED04AAB8CBBCF85A4278A404CFD2FE29 (void);
// 0x0000005A RosSharp.Urdf.Link/Inertial/Inertia RosSharp.Urdf.UrdfInertial::ToRosCoordinates(RosSharp.Urdf.Link/Inertial/Inertia)
extern void UrdfInertial_ToRosCoordinates_m57743F1012088C1A48D8F88B1BB13C4F38D3C38A (void);
// 0x0000005B System.Void RosSharp.Urdf.UrdfInertial::.ctor()
extern void UrdfInertial__ctor_m1764ACEB6E57315211DB70DDC242E11691C1F3D2 (void);
// 0x0000005C RosSharp.Urdf.UrdfJoint/JointTypes RosSharp.Urdf.UrdfJoint::get_JointType()
extern void UrdfJoint_get_JointType_m1C966143B9AC8783D9CF42A89B852C651ED8AECD (void);
// 0x0000005D System.Void RosSharp.Urdf.UrdfJoint::set_JointType(RosSharp.Urdf.UrdfJoint/JointTypes)
extern void UrdfJoint_set_JointType_m8196165979DF2C4470456D96AE821A80FAD45D75 (void);
// 0x0000005E System.Boolean RosSharp.Urdf.UrdfJoint::get_IsRevoluteOrContinuous()
extern void UrdfJoint_get_IsRevoluteOrContinuous_mD3B8E8C89951AD4C44E2C6BCC98206D6BFA8E012 (void);
// 0x0000005F System.Void RosSharp.Urdf.UrdfJoint::Create(UnityEngine.GameObject,RosSharp.Urdf.UrdfJoint/JointTypes,RosSharp.Urdf.Joint)
extern void UrdfJoint_Create_m8B715E80716C44A94609F5BCF897867D05774C2C (void);
// 0x00000060 RosSharp.Urdf.UrdfJoint RosSharp.Urdf.UrdfJoint::AddCorrectJointType(UnityEngine.GameObject,RosSharp.Urdf.UrdfJoint/JointTypes)
extern void UrdfJoint_AddCorrectJointType_m4ED60BD9DEC372F4E1E5C401FC7926B50CF178DF (void);
// 0x00000061 System.Void RosSharp.Urdf.UrdfJoint::ChangeJointType(UnityEngine.GameObject,RosSharp.Urdf.UrdfJoint/JointTypes)
extern void UrdfJoint_ChangeJointType_mFA8EF3A6B730D6C04296E9DA77B401A97109FB77 (void);
// 0x00000062 System.Void RosSharp.Urdf.UrdfJoint::Start()
extern void UrdfJoint_Start_m8EDF4E78607ABB306CCA2CB1BAC436C75AF3537A (void);
// 0x00000063 System.Single RosSharp.Urdf.UrdfJoint::GetPosition()
extern void UrdfJoint_GetPosition_mB2DC1C68EE96BC04B9AA0C103083D0C12B0B0328 (void);
// 0x00000064 System.Single RosSharp.Urdf.UrdfJoint::GetVelocity()
extern void UrdfJoint_GetVelocity_mFC42C346D0B9DE4E8F0D55885AFDA046FE8730F6 (void);
// 0x00000065 System.Single RosSharp.Urdf.UrdfJoint::GetEffort()
extern void UrdfJoint_GetEffort_m7E51E92B83209920696ADB4972BF7F1F40BDB6B9 (void);
// 0x00000066 System.Void RosSharp.Urdf.UrdfJoint::UpdateJointState(System.Single)
extern void UrdfJoint_UpdateJointState_m9565C0AA4FF402C1BF47D58A73D0734F8C7C1D08 (void);
// 0x00000067 System.Void RosSharp.Urdf.UrdfJoint::OnUpdateJointState(System.Single)
extern void UrdfJoint_OnUpdateJointState_m71A88F42A534CEE00808963DEA7DC1AED3E3D478 (void);
// 0x00000068 RosSharp.Urdf.UrdfJoint/JointTypes RosSharp.Urdf.UrdfJoint::GetJointType(System.String)
extern void UrdfJoint_GetJointType_mD37A3A094BF68F75E602D068EADA9FAF182F7668 (void);
// 0x00000069 System.Void RosSharp.Urdf.UrdfJoint::ImportJointData(RosSharp.Urdf.Joint)
extern void UrdfJoint_ImportJointData_m8DD46CB18C937D8F9F2381CAF7ABA7DC18E2CC5B (void);
// 0x0000006A UnityEngine.Vector3 RosSharp.Urdf.UrdfJoint::GetAxis(RosSharp.Urdf.Joint/Axis)
extern void UrdfJoint_GetAxis_m377B2DD77DB716FFB6C810B50F8010CFB75157DE (void);
// 0x0000006B UnityEngine.Vector3 RosSharp.Urdf.UrdfJoint::GetDefaultAxis()
extern void UrdfJoint_GetDefaultAxis_mC15D10371B3BA2E4BBCBD3A1A07F3663CB49E9B1 (void);
// 0x0000006C UnityEngine.JointDrive RosSharp.Urdf.UrdfJoint::GetJointDrive(RosSharp.Urdf.Joint/Dynamics)
extern void UrdfJoint_GetJointDrive_m7B41BF11296F4C80374037E7EDB2B80D7A09EF28 (void);
// 0x0000006D UnityEngine.JointSpring RosSharp.Urdf.UrdfJoint::GetJointSpring(RosSharp.Urdf.Joint/Dynamics)
extern void UrdfJoint_GetJointSpring_mD8B0BD896978B37D2FCF6525348337EEA95AD38C (void);
// 0x0000006E UnityEngine.SoftJointLimit RosSharp.Urdf.UrdfJoint::GetLinearLimit(RosSharp.Urdf.Joint/Limit)
extern void UrdfJoint_GetLinearLimit_m606F9CCA7E468E69D49141C455D719FDE47E3F12 (void);
// 0x0000006F RosSharp.Urdf.Joint RosSharp.Urdf.UrdfJoint::ExportJointData()
extern void UrdfJoint_ExportJointData_m272A5C87509D955F7E39B716A800430589919F77 (void);
// 0x00000070 RosSharp.Urdf.Joint RosSharp.Urdf.UrdfJoint::ExportDefaultJoint(UnityEngine.Transform)
extern void UrdfJoint_ExportDefaultJoint_mD163D9E86B5721B2439C724081A46DCD47E26C95 (void);
// 0x00000071 RosSharp.Urdf.Joint RosSharp.Urdf.UrdfJoint::ExportSpecificJointData(RosSharp.Urdf.Joint)
extern void UrdfJoint_ExportSpecificJointData_m1AE373ABFED1C36A50123AC59669BF435233C6A2 (void);
// 0x00000072 RosSharp.Urdf.Joint/Limit RosSharp.Urdf.UrdfJoint::ExportLimitData()
extern void UrdfJoint_ExportLimitData_m2E38139DEF3B52D54A1FB05FE7671E74F0F8EAF9 (void);
// 0x00000073 System.Boolean RosSharp.Urdf.UrdfJoint::AreLimitsCorrect()
extern void UrdfJoint_AreLimitsCorrect_m5655EA1722FCF9F613C2021D3A65B0AB4E136E66 (void);
// 0x00000074 System.Boolean RosSharp.Urdf.UrdfJoint::IsJointAxisDefined()
extern void UrdfJoint_IsJointAxisDefined_m3C165ACEE4CDF7E95438F2B1112D9EFD7FCA9444 (void);
// 0x00000075 System.Void RosSharp.Urdf.UrdfJoint::GenerateUniqueJointName()
extern void UrdfJoint_GenerateUniqueJointName_m5D518038028AA0CA8E0BDF001F45C1C6605BE0F6 (void);
// 0x00000076 RosSharp.Urdf.Joint/Axis RosSharp.Urdf.UrdfJoint::GetAxisData(UnityEngine.Vector3)
extern void UrdfJoint_GetAxisData_mEE13E04B9C01177B927D624E0480ED064F418074 (void);
// 0x00000077 System.Boolean RosSharp.Urdf.UrdfJoint::IsAnchorTransformed()
extern void UrdfJoint_IsAnchorTransformed_m1ED8D68348C8D83545597E3395C2A16ABF6301B7 (void);
// 0x00000078 System.Void RosSharp.Urdf.UrdfJoint::CheckForUrdfCompatibility()
extern void UrdfJoint_CheckForUrdfCompatibility_m3681EDA6606145ED2382047BEC84A8D9F7E28180 (void);
// 0x00000079 System.Void RosSharp.Urdf.UrdfJoint::.ctor()
extern void UrdfJoint__ctor_m207B2E8311C17443DC01FF33EC8227FCC9202D48 (void);
// 0x0000007A RosSharp.Urdf.UrdfJoint RosSharp.Urdf.UrdfJointContinuous::Create(UnityEngine.GameObject)
extern void UrdfJointContinuous_Create_mE1C39B3DFE030EB4FE216E6AE54970DC3F120B46 (void);
// 0x0000007B System.Single RosSharp.Urdf.UrdfJointContinuous::GetPosition()
extern void UrdfJointContinuous_GetPosition_m656A4200647E6F18CB72A1E9A5C66BA0BB0A8AAF (void);
// 0x0000007C System.Single RosSharp.Urdf.UrdfJointContinuous::GetVelocity()
extern void UrdfJointContinuous_GetVelocity_mD66D957E869D42E5E5DBC33E85520437A54C5404 (void);
// 0x0000007D System.Single RosSharp.Urdf.UrdfJointContinuous::GetEffort()
extern void UrdfJointContinuous_GetEffort_m56D7DB7FBD4653BDD28348BC9B7B9A49BF5EB2E4 (void);
// 0x0000007E System.Void RosSharp.Urdf.UrdfJointContinuous::OnUpdateJointState(System.Single)
extern void UrdfJointContinuous_OnUpdateJointState_m4B7F7285CDE156AD1175BE7E9D0E70DD6097FEA7 (void);
// 0x0000007F System.Void RosSharp.Urdf.UrdfJointContinuous::ImportJointData(RosSharp.Urdf.Joint)
extern void UrdfJointContinuous_ImportJointData_mE99C13F2A1BFA6ADF31EED8903CF68D26DA6F4E1 (void);
// 0x00000080 RosSharp.Urdf.Joint RosSharp.Urdf.UrdfJointContinuous::ExportSpecificJointData(RosSharp.Urdf.Joint)
extern void UrdfJointContinuous_ExportSpecificJointData_m38F806A4F50664119AD1D7CDC7132DFD9C8EC47F (void);
// 0x00000081 System.Void RosSharp.Urdf.UrdfJointContinuous::.ctor()
extern void UrdfJointContinuous__ctor_m9AF00FDF5791EECE4E52F33A915D0E5634CFE790 (void);
// 0x00000082 RosSharp.Urdf.UrdfJoint RosSharp.Urdf.UrdfJointFixed::Create(UnityEngine.GameObject)
extern void UrdfJointFixed_Create_m046048B5B65C299643769C19775AF2117F67C47A (void);
// 0x00000083 System.Boolean RosSharp.Urdf.UrdfJointFixed::IsJointAxisDefined()
extern void UrdfJointFixed_IsJointAxisDefined_mBD6D6BBA26AA39DA470586A2ED0F2FC6F4029FDA (void);
// 0x00000084 System.Void RosSharp.Urdf.UrdfJointFixed::.ctor()
extern void UrdfJointFixed__ctor_m42013A30E1426B02B814D88262EBF29F50411ABE (void);
// 0x00000085 RosSharp.Urdf.UrdfJoint RosSharp.Urdf.UrdfJointFloating::Create(UnityEngine.GameObject)
extern void UrdfJointFloating_Create_mC659A9CB431AEC673FD93987975CF4C0EEA52B36 (void);
// 0x00000086 System.Single RosSharp.Urdf.UrdfJointFloating::GetPosition()
extern void UrdfJointFloating_GetPosition_m3239CCF3094FBEF59AB0A587B8DFC84C6941C416 (void);
// 0x00000087 System.Boolean RosSharp.Urdf.UrdfJointFloating::IsJointAxisDefined()
extern void UrdfJointFloating_IsJointAxisDefined_m6B48E8462BA956E46B34B1CCDD452B7D5D7DF0E9 (void);
// 0x00000088 System.Void RosSharp.Urdf.UrdfJointFloating::.ctor()
extern void UrdfJointFloating__ctor_mE4E8E58678137F56E4CD53345E070E64516F4C69 (void);
// 0x00000089 RosSharp.Urdf.UrdfJoint RosSharp.Urdf.UrdfJointPlanar::Create(UnityEngine.GameObject)
extern void UrdfJointPlanar_Create_m94E6C67AD61669B59101B93AE3FF17BE8C3B15BD (void);
// 0x0000008A System.Single RosSharp.Urdf.UrdfJointPlanar::GetPosition()
extern void UrdfJointPlanar_GetPosition_m8B9537C0CD369F4A3E090420C6C45CA4495660A3 (void);
// 0x0000008B System.Void RosSharp.Urdf.UrdfJointPlanar::ImportJointData(RosSharp.Urdf.Joint)
extern void UrdfJointPlanar_ImportJointData_m38B69EBDDD033AD14ECE265E333D313F06374A3F (void);
// 0x0000008C RosSharp.Urdf.Joint RosSharp.Urdf.UrdfJointPlanar::ExportSpecificJointData(RosSharp.Urdf.Joint)
extern void UrdfJointPlanar_ExportSpecificJointData_mDE08CC0B1E907F19CEC663CABDF0F37583D92433 (void);
// 0x0000008D RosSharp.Urdf.Joint/Limit RosSharp.Urdf.UrdfJointPlanar::ExportLimitData()
extern void UrdfJointPlanar_ExportLimitData_m2E97A443EE2982723333417AB2B65CC63D63E3D7 (void);
// 0x0000008E System.Boolean RosSharp.Urdf.UrdfJointPlanar::AreLimitsCorrect()
extern void UrdfJointPlanar_AreLimitsCorrect_mCAD9785AA8C7CA1FCA19C7D5B63ED90CB81E9861 (void);
// 0x0000008F System.Boolean RosSharp.Urdf.UrdfJointPlanar::IsJointAxisDefined()
extern void UrdfJointPlanar_IsJointAxisDefined_m060D14AFA5CF300A187A219C79DD956BAF804352 (void);
// 0x00000090 System.Void RosSharp.Urdf.UrdfJointPlanar::.ctor()
extern void UrdfJointPlanar__ctor_m720D6F5E034E26B850207B1614BC733515473F42 (void);
// 0x00000091 RosSharp.Urdf.UrdfJoint RosSharp.Urdf.UrdfJointPrismatic::Create(UnityEngine.GameObject)
extern void UrdfJointPrismatic_Create_mFF2B6C608245FA9AD92C36AE38F24DFEE61CAD6D (void);
// 0x00000092 System.Single RosSharp.Urdf.UrdfJointPrismatic::GetPosition()
extern void UrdfJointPrismatic_GetPosition_mABB4772308FB32F4C5ECE5ABFE0223701C1A0D0B (void);
// 0x00000093 System.Void RosSharp.Urdf.UrdfJointPrismatic::OnUpdateJointState(System.Single)
extern void UrdfJointPrismatic_OnUpdateJointState_m8D5848A53B1BB8EFB776A6F43BCB60853D0C5EBF (void);
// 0x00000094 System.Void RosSharp.Urdf.UrdfJointPrismatic::ImportJointData(RosSharp.Urdf.Joint)
extern void UrdfJointPrismatic_ImportJointData_m81EA5FD18D6639EFA6FF2EE99FAB723FADB68B27 (void);
// 0x00000095 RosSharp.Urdf.Joint RosSharp.Urdf.UrdfJointPrismatic::ExportSpecificJointData(RosSharp.Urdf.Joint)
extern void UrdfJointPrismatic_ExportSpecificJointData_m0744FF9A3D68A6EC89877B1F01AFBB94CC570815 (void);
// 0x00000096 System.Boolean RosSharp.Urdf.UrdfJointPrismatic::AreLimitsCorrect()
extern void UrdfJointPrismatic_AreLimitsCorrect_mE5CB315B6809F7C4A56CF3C555E10CB5AE506DD1 (void);
// 0x00000097 RosSharp.Urdf.Joint/Limit RosSharp.Urdf.UrdfJointPrismatic::ExportLimitData()
extern void UrdfJointPrismatic_ExportLimitData_m4AAC6A0F8A9B1236C814BACB136CC6ECC6CA0A61 (void);
// 0x00000098 System.Void RosSharp.Urdf.UrdfJointPrismatic::.ctor()
extern void UrdfJointPrismatic__ctor_m148DD4B601DC0BD3BF46EDE78BF751DD64A3F0CC (void);
// 0x00000099 RosSharp.Urdf.UrdfJoint RosSharp.Urdf.UrdfJointRevolute::Create(UnityEngine.GameObject)
extern void UrdfJointRevolute_Create_m4C963609227FB6653139A7CAD41DC627E786B46C (void);
// 0x0000009A System.Single RosSharp.Urdf.UrdfJointRevolute::GetPosition()
extern void UrdfJointRevolute_GetPosition_mFE2B3F946181437081F1DCB1677C5B0D21CA3B48 (void);
// 0x0000009B System.Single RosSharp.Urdf.UrdfJointRevolute::GetVelocity()
extern void UrdfJointRevolute_GetVelocity_m247827D372C2206AD62BFD1D909C9EEBAA64A139 (void);
// 0x0000009C System.Single RosSharp.Urdf.UrdfJointRevolute::GetEffort()
extern void UrdfJointRevolute_GetEffort_m91E5226824DB706554B59C1DEB6AB91C37EF8862 (void);
// 0x0000009D System.Void RosSharp.Urdf.UrdfJointRevolute::OnUpdateJointState(System.Single)
extern void UrdfJointRevolute_OnUpdateJointState_m182D4514462BEC0A9D2EFFF8A28EB4CC9779AAAA (void);
// 0x0000009E System.Void RosSharp.Urdf.UrdfJointRevolute::ImportJointData(RosSharp.Urdf.Joint)
extern void UrdfJointRevolute_ImportJointData_mD73C6F53C0208524F2E0250B76E9339A779AE0DA (void);
// 0x0000009F RosSharp.Urdf.Joint RosSharp.Urdf.UrdfJointRevolute::ExportSpecificJointData(RosSharp.Urdf.Joint)
extern void UrdfJointRevolute_ExportSpecificJointData_m4420404CF9793A64DFB6554C9A17A7B8F34BA370 (void);
// 0x000000A0 System.Boolean RosSharp.Urdf.UrdfJointRevolute::AreLimitsCorrect()
extern void UrdfJointRevolute_AreLimitsCorrect_m21276E808A7F1ED5BCCC618989F32EF359FE6369 (void);
// 0x000000A1 RosSharp.Urdf.Joint/Limit RosSharp.Urdf.UrdfJointRevolute::ExportLimitData()
extern void UrdfJointRevolute_ExportLimitData_m1E340BD34D16DA5E8000516A200C12961D1B1B8D (void);
// 0x000000A2 System.Void RosSharp.Urdf.UrdfJointRevolute::.ctor()
extern void UrdfJointRevolute__ctor_m0AFD9DF73539DB8DAACCAA6E05C22605F724DC3D (void);
// 0x000000A3 System.Void RosSharp.Urdf.UrdfLink::.ctor()
extern void UrdfLink__ctor_mDC1F5B4F00E1468FF62C2BB0CD3FB33A2D843CB6 (void);
// 0x000000A4 System.Void RosSharp.Urdf.UrdfOrigin::ImportOriginData(UnityEngine.Transform,RosSharp.Urdf.Origin)
extern void UrdfOrigin_ImportOriginData_mEF583358C0F7F7D0444D62314B584F8D1A745DE7 (void);
// 0x000000A5 UnityEngine.Vector3 RosSharp.Urdf.UrdfOrigin::GetPositionFromUrdf(RosSharp.Urdf.Origin)
extern void UrdfOrigin_GetPositionFromUrdf_m77EF016FCEC3B2F9E5195A8E7F1E9426C8B008CA (void);
// 0x000000A6 UnityEngine.Vector3 RosSharp.Urdf.UrdfOrigin::GetRotationFromUrdf(RosSharp.Urdf.Origin)
extern void UrdfOrigin_GetRotationFromUrdf_mC9F192CDFD432B762009EC855A2A4858F237870C (void);
// 0x000000A7 RosSharp.Urdf.Origin RosSharp.Urdf.UrdfOrigin::ExportOriginData(UnityEngine.Transform)
extern void UrdfOrigin_ExportOriginData_mD1AB04662F1F7B7B91240E29542784E5D1C988EF (void);
// 0x000000A8 System.Double[] RosSharp.Urdf.UrdfOrigin::ExportXyzData(UnityEngine.Transform)
extern void UrdfOrigin_ExportXyzData_m4C5BAE09BD06C1553527E3F0A6CD08B3BD4C6D1A (void);
// 0x000000A9 System.Double[] RosSharp.Urdf.UrdfOrigin::ExportRpyData(UnityEngine.Transform)
extern void UrdfOrigin_ExportRpyData_mE753B5C0414F99DA1F7FEA742CDA0C6F9D7F43D8 (void);
// 0x000000AA System.Void RosSharp.Urdf.UrdfPlugin::Create(UnityEngine.Transform,RosSharp.Urdf.Plugin)
extern void UrdfPlugin_Create_mEF32FC7631BF4388C3A78E55ED5B4C6D32F7BC0E (void);
// 0x000000AB RosSharp.Urdf.Plugin RosSharp.Urdf.UrdfPlugin::ExportPluginData()
extern void UrdfPlugin_ExportPluginData_m0A59A4244303EFDD21BEC86C761447709478172F (void);
// 0x000000AC System.Void RosSharp.Urdf.UrdfPlugin::.ctor()
extern void UrdfPlugin__ctor_m339A815FD14C72D76749433AD4346A37818E7CBC (void);
// 0x000000AD System.Void RosSharp.Urdf.UrdfPlugins::Create(UnityEngine.Transform,System.Collections.Generic.List`1<RosSharp.Urdf.Plugin>)
extern void UrdfPlugins_Create_mC80611E2F177E45E64CC7E52E8AE297DB0C8A06B (void);
// 0x000000AE System.Collections.Generic.List`1<RosSharp.Urdf.Plugin> RosSharp.Urdf.UrdfPlugins::ExportPluginsData()
extern void UrdfPlugins_ExportPluginsData_m9CF32A0D0A2C700F8292417F6BF33A48E24AC048 (void);
// 0x000000AF System.Void RosSharp.Urdf.UrdfPlugins::.ctor()
extern void UrdfPlugins__ctor_m89B9D44F4F5DBCF953D090BD552BB3E509221B96 (void);
// 0x000000B0 System.Void RosSharp.Urdf.UrdfPlugins/<>c::.cctor()
extern void U3CU3Ec__cctor_mC125EF0C21454C544D60F373535F88B353D8B895 (void);
// 0x000000B1 System.Void RosSharp.Urdf.UrdfPlugins/<>c::.ctor()
extern void U3CU3Ec__ctor_mDB61C170875ADF820A910BEE3239EC8C36A35CF9 (void);
// 0x000000B2 RosSharp.Urdf.Plugin RosSharp.Urdf.UrdfPlugins/<>c::<ExportPluginsData>b__1_0(RosSharp.Urdf.UrdfPlugin)
extern void U3CU3Ec_U3CExportPluginsDataU3Eb__1_0_mF8AD33592A69B12C9D6DD8D0A4A2F4410D084FC4 (void);
// 0x000000B3 System.Boolean RosSharp.Urdf.UrdfPlugins/<>c::<ExportPluginsData>b__1_1(RosSharp.Urdf.Plugin)
extern void U3CU3Ec_U3CExportPluginsDataU3Eb__1_1_m51D2C8E264F4FAC62788169AB15EA8F131F17A95 (void);
// 0x000000B4 System.Void RosSharp.Urdf.UrdfRobot::SetCollidersConvex(System.Boolean)
extern void UrdfRobot_SetCollidersConvex_mB196A7499C1CAF141935471A0D805DF7030C3A6E (void);
// 0x000000B5 System.Void RosSharp.Urdf.UrdfRobot::SetRigidbodiesIsKinematic(System.Boolean)
extern void UrdfRobot_SetRigidbodiesIsKinematic_mB700865E1A5A51973E38D972735A7625BE336FE7 (void);
// 0x000000B6 System.Void RosSharp.Urdf.UrdfRobot::SetUseUrdfInertiaData(System.Boolean)
extern void UrdfRobot_SetUseUrdfInertiaData_mFF6EDD5542E13344E4E5614BC4B63AFA9BB58B64 (void);
// 0x000000B7 System.Void RosSharp.Urdf.UrdfRobot::SetRigidbodiesUseGravity(System.Boolean)
extern void UrdfRobot_SetRigidbodiesUseGravity_m9A7244771AB4D4D98E6279B9D886F451A354CC82 (void);
// 0x000000B8 System.Void RosSharp.Urdf.UrdfRobot::GenerateUniqueJointNames()
extern void UrdfRobot_GenerateUniqueJointNames_m819F606B9561D1A21FE4E2BF63DDCF4C10C2DC08 (void);
// 0x000000B9 System.Void RosSharp.Urdf.UrdfRobot::.ctor()
extern void UrdfRobot__ctor_mE18E0295E8CBF9F475F04EA3009B780D4AC80C82 (void);
// 0x000000BA System.Void RosSharp.Urdf.UrdfVisual::.ctor()
extern void UrdfVisual__ctor_mC36EB6BE1DCFB070DD0F21BB89E3765DFA38819F (void);
// 0x000000BB System.Void RosSharp.Urdf.UrdfVisuals::.ctor()
extern void UrdfVisuals__ctor_m3E00361E983E49FB3AB6D98B406A8DDE822CBF6C (void);
// 0x000000BC System.Void RosSharp.RosBridgeClient.HeaderExtensions::Update(RosSharp.RosBridgeClient.MessageTypes.Std.Header)
extern void HeaderExtensions_Update_m4DEAC48EBEB22EBA4400060CD3977F7DC512FE6A (void);
// 0x000000BD System.Void RosSharp.RosBridgeClient.HeaderExtensions::.cctor()
extern void HeaderExtensions__cctor_m6BE4656E0F3DF4C46384232EC169164DC42FC1EB (void);
// 0x000000BE System.Void RosSharp.RosBridgeClient.JointStatePatcher::SetPublishJointStates(System.Boolean)
extern void JointStatePatcher_SetPublishJointStates_mABDA01758571F7C0DA12BBCA23B04BBC70B63325 (void);
// 0x000000BF System.Void RosSharp.RosBridgeClient.JointStatePatcher::SetSubscribeJointStates(System.Boolean)
extern void JointStatePatcher_SetSubscribeJointStates_mF94EA205BAEE2C158A31506767E3BEE793E8958F (void);
// 0x000000C0 System.Void RosSharp.RosBridgeClient.JointStatePatcher::.ctor()
extern void JointStatePatcher__ctor_m3A620B25FD1176D20F6F793EF86220A13DA6340E (void);
// 0x000000C1 System.Void RosSharp.RosBridgeClient.JointStateReader::Start()
extern void JointStateReader_Start_m6E340B7F91D61DAFD64E92954C51BEFE6B5EEBDA (void);
// 0x000000C2 System.Void RosSharp.RosBridgeClient.JointStateReader::Read(System.String&,System.Single&,System.Single&,System.Single&)
extern void JointStateReader_Read_m9CF7A1F1B029935A27471EB711E7FD85ECCF3E6D (void);
// 0x000000C3 System.Void RosSharp.RosBridgeClient.JointStateReader::.ctor()
extern void JointStateReader__ctor_mDDFF4F94344AADA1B953EF1D014A08194B8CED21 (void);
// 0x000000C4 System.Void RosSharp.RosBridgeClient.JointStateWriter::Start()
extern void JointStateWriter_Start_m2DAC1165DF4632CAF1E81E5AC0D91CF3EB98F753 (void);
// 0x000000C5 System.Void RosSharp.RosBridgeClient.JointStateWriter::Update()
extern void JointStateWriter_Update_m0D57D2F9ECEA807957552BCD4F25762016A9C401 (void);
// 0x000000C6 System.Void RosSharp.RosBridgeClient.JointStateWriter::WriteUpdate()
extern void JointStateWriter_WriteUpdate_mAD78FE0A83A4D54739293CDADF909AA2C82E9826 (void);
// 0x000000C7 System.Void RosSharp.RosBridgeClient.JointStateWriter::Write(System.Single)
extern void JointStateWriter_Write_m2D9582D61B53E10F825BCAB2F836DC4631C20B72 (void);
// 0x000000C8 System.Void RosSharp.RosBridgeClient.JointStateWriter::.ctor()
extern void JointStateWriter__ctor_m5391E355FFB1ADDE13DCC34208A1E91B171AB630 (void);
// 0x000000C9 System.Void RosSharp.RosBridgeClient.JoyAxisInputPasser::Start()
extern void JoyAxisInputPasser_Start_mFA84A945B0B4F48197A9EB682267095BC3468459 (void);
// 0x000000CA System.Void RosSharp.RosBridgeClient.JoyAxisInputPasser::Update()
extern void JoyAxisInputPasser_Update_mEDCD98F51525BA0EA17408CF41D5A7179A12A815 (void);
// 0x000000CB System.Void RosSharp.RosBridgeClient.JoyAxisInputPasser::.ctor()
extern void JoyAxisInputPasser__ctor_m00954BF3F1091F11584AEB5D0CC9A5EA1F9CD14C (void);
// 0x000000CC System.Void RosSharp.RosBridgeClient.JoyAxisJointMotorWriter::Start()
extern void JoyAxisJointMotorWriter_Start_m7D973A248BA39897C47503C8D45C63091B0390F7 (void);
// 0x000000CD System.Void RosSharp.RosBridgeClient.JoyAxisJointMotorWriter::Update()
extern void JoyAxisJointMotorWriter_Update_mEACDC13FCFD5B76FA1D332D30A761B3AFFE4262C (void);
// 0x000000CE System.Void RosSharp.RosBridgeClient.JoyAxisJointMotorWriter::ProcessMessage()
extern void JoyAxisJointMotorWriter_ProcessMessage_mE019CB99A72988F88E90290D048F991DB53784A0 (void);
// 0x000000CF System.Void RosSharp.RosBridgeClient.JoyAxisJointMotorWriter::Write(System.Single)
extern void JoyAxisJointMotorWriter_Write_m7578AE3D748ECD6AB159D3EDE60ACE4F4118F205 (void);
// 0x000000D0 System.Void RosSharp.RosBridgeClient.JoyAxisJointMotorWriter::.ctor()
extern void JoyAxisJointMotorWriter__ctor_mAA66AE53DA17C352D29E359871151794B23E27EE (void);
// 0x000000D1 System.Void RosSharp.RosBridgeClient.JoyAxisJointTransformWriter::Start()
extern void JoyAxisJointTransformWriter_Start_m87013FE3CE6F6470FB84D630EB23B7BAD8FFF3D9 (void);
// 0x000000D2 System.Void RosSharp.RosBridgeClient.JoyAxisJointTransformWriter::ApplyLimits()
extern void JoyAxisJointTransformWriter_ApplyLimits_m6066DAFB054ABAC0931C4892FD5C1615BA066F08 (void);
// 0x000000D3 System.Void RosSharp.RosBridgeClient.JoyAxisJointTransformWriter::Update()
extern void JoyAxisJointTransformWriter_Update_mB5254B1FDA3399C4EAC68EEA38DD90BD15358942 (void);
// 0x000000D4 System.Void RosSharp.RosBridgeClient.JoyAxisJointTransformWriter::ProcessMessage()
extern void JoyAxisJointTransformWriter_ProcessMessage_m5B5C492C36074B2FF724622109D171BED8BBFDD0 (void);
// 0x000000D5 System.Void RosSharp.RosBridgeClient.JoyAxisJointTransformWriter::Write(System.Single)
extern void JoyAxisJointTransformWriter_Write_mDF302C82BAF6B5E4DB874DD8808F6710503CEB34 (void);
// 0x000000D6 System.Void RosSharp.RosBridgeClient.JoyAxisJointTransformWriter::.ctor()
extern void JoyAxisJointTransformWriter__ctor_mD42420996750B6B019AC9C8C4E934BC75C6CDD1F (void);
// 0x000000D7 System.Single RosSharp.RosBridgeClient.JoyAxisReader::Read()
extern void JoyAxisReader_Read_mEDF02F4243E9157760679963438D24BDBFE9A10F (void);
// 0x000000D8 System.Void RosSharp.RosBridgeClient.JoyAxisReader::.ctor()
extern void JoyAxisReader__ctor_mC55AA058504740F49A4FB26FBDB3880ED11F869B (void);
// 0x000000D9 System.Void RosSharp.RosBridgeClient.JoyAxisWriter::Write(System.Single)
// 0x000000DA System.Void RosSharp.RosBridgeClient.JoyAxisWriter::.ctor()
extern void JoyAxisWriter__ctor_mC44849F84B490B377D178FB9BBEE63DDBA647B12 (void);
// 0x000000DB System.Boolean RosSharp.RosBridgeClient.JoyButtonReader::Read()
extern void JoyButtonReader_Read_m054C6A69060C966B3292589E6D7ADC6A34507E88 (void);
// 0x000000DC System.Void RosSharp.RosBridgeClient.JoyButtonReader::.ctor()
extern void JoyButtonReader__ctor_mE893F35E1D15F8F02E9CBE51DD91CC0E7E12490B (void);
// 0x000000DD System.Void RosSharp.RosBridgeClient.JoyButtonWriter::Write(System.Int32)
// 0x000000DE System.Void RosSharp.RosBridgeClient.JoyButtonWriter::.ctor()
extern void JoyButtonWriter__ctor_m64CA3ED90E7691752100974C4007FAEDDCA64B06 (void);
// 0x000000DF System.Void RosSharp.RosBridgeClient.LaserScanReader::Start()
extern void LaserScanReader_Start_m8B2563F582A63D4DF11F70318B5497FD6A04FD9A (void);
// 0x000000E0 System.Single[] RosSharp.RosBridgeClient.LaserScanReader::Scan()
extern void LaserScanReader_Scan_m79A4B76F8326E591E124E03EDA51D7E3AFD55FDB (void);
// 0x000000E1 System.Void RosSharp.RosBridgeClient.LaserScanReader::MeasureDistance()
extern void LaserScanReader_MeasureDistance_mAB31F7634AC27C2A4A2E9E79F6D22C867B5A8EE4 (void);
// 0x000000E2 System.Void RosSharp.RosBridgeClient.LaserScanReader::.ctor()
extern void LaserScanReader__ctor_m206FA6C04878C6AD878328D56E63034C2C4A3D27 (void);
// 0x000000E3 System.Void RosSharp.RosBridgeClient.LaserScanWriter::Update()
extern void LaserScanWriter_Update_mF4A603A8C05741AC35C2ACB3FE97C76C90B5B83E (void);
// 0x000000E4 System.Void RosSharp.RosBridgeClient.LaserScanWriter::Write(RosSharp.RosBridgeClient.MessageTypes.Sensor.LaserScan)
extern void LaserScanWriter_Write_m9FFDDA606BF7904DACF33D055184D8B9A2FBBE5B (void);
// 0x000000E5 System.Void RosSharp.RosBridgeClient.LaserScanWriter::.ctor()
extern void LaserScanWriter__ctor_m9FD31A8392EBD0E2BED67C23155619FED7AE0589 (void);
// 0x000000E6 System.Boolean RosSharp.RosBridgeClient.GetParamServiceProvider::ServiceCallHandler(RosSharp.RosBridgeClient.MessageTypes.Rosapi.GetParamRequest,RosSharp.RosBridgeClient.MessageTypes.Rosapi.GetParamResponse&)
extern void GetParamServiceProvider_ServiceCallHandler_m3423A16E0FEA7ADC85BF8B8D67906963E5636183 (void);
// 0x000000E7 System.Void RosSharp.RosBridgeClient.GetParamServiceProvider::.ctor()
extern void GetParamServiceProvider__ctor_m558B2C91B70231B0582423EE69C5673BD5293A50 (void);
// 0x000000E8 System.Void RosSharp.RosBridgeClient.ImagePublisher::Start()
extern void ImagePublisher_Start_m67A3E75572942369FC1E4AC4CF2F079FA7494DF7 (void);
// 0x000000E9 System.Void RosSharp.RosBridgeClient.ImagePublisher::UpdateImage(UnityEngine.Camera)
extern void ImagePublisher_UpdateImage_m2291765CC1D3A91EE5FEFE95E20D1F6B3BD29FEF (void);
// 0x000000EA System.Void RosSharp.RosBridgeClient.ImagePublisher::InitializeGameObject()
extern void ImagePublisher_InitializeGameObject_mC474EFE9B8464B7DC53A15AA297CF0EFB1A398EB (void);
// 0x000000EB System.Void RosSharp.RosBridgeClient.ImagePublisher::InitializeMessage()
extern void ImagePublisher_InitializeMessage_m38C65D1F792C7792B1C77B597ECA72CC456ED5DF (void);
// 0x000000EC System.Void RosSharp.RosBridgeClient.ImagePublisher::UpdateMessage()
extern void ImagePublisher_UpdateMessage_mD4BAE26E9F4BDBC2D78FA1F1F070752BEC81B8C5 (void);
// 0x000000ED System.Void RosSharp.RosBridgeClient.ImagePublisher::.ctor()
extern void ImagePublisher__ctor_mA2A66F6BEDA545C26292C4954ED3BAEB7886F1E4 (void);
// 0x000000EE System.Void RosSharp.RosBridgeClient.ImageSubscriber::Start()
extern void ImageSubscriber_Start_mD67094ED4CB99E6C320D966B940867DE5EEED08D (void);
// 0x000000EF System.Void RosSharp.RosBridgeClient.ImageSubscriber::Update()
extern void ImageSubscriber_Update_m47094FAD2142A197D46DC32B9FFB6E1AAC7ADC04 (void);
// 0x000000F0 System.Void RosSharp.RosBridgeClient.ImageSubscriber::ReceiveMessage(RosSharp.RosBridgeClient.MessageTypes.Sensor.CompressedImage)
extern void ImageSubscriber_ReceiveMessage_m030609D17F23129A6790DEF0BFD54D546EBFDF00 (void);
// 0x000000F1 System.Void RosSharp.RosBridgeClient.ImageSubscriber::ProcessMessage()
extern void ImageSubscriber_ProcessMessage_m10E95382F3FE1BB86BABB0D0C8ADC66CE6A33DCF (void);
// 0x000000F2 System.Void RosSharp.RosBridgeClient.ImageSubscriber::.ctor()
extern void ImageSubscriber__ctor_mABFDB8D0A2150BB34E55E45BE9B8002AD18563C8 (void);
// 0x000000F3 System.Void RosSharp.RosBridgeClient.JointStatePublisher::Start()
extern void JointStatePublisher_Start_m3C4944420983070BE12820D4BE41DD1B011FA70F (void);
// 0x000000F4 System.Void RosSharp.RosBridgeClient.JointStatePublisher::FixedUpdate()
extern void JointStatePublisher_FixedUpdate_m4A6F030633F1589299ED3AB21F111B8D84ADA0AB (void);
// 0x000000F5 System.Void RosSharp.RosBridgeClient.JointStatePublisher::InitializeMessage()
extern void JointStatePublisher_InitializeMessage_m6643F8A92D59E61376B17C08F3CB1E1D9C741E54 (void);
// 0x000000F6 System.Void RosSharp.RosBridgeClient.JointStatePublisher::UpdateMessage()
extern void JointStatePublisher_UpdateMessage_m6EC84B84134ADB515FBB71C574DE0B9C90A9D433 (void);
// 0x000000F7 System.Void RosSharp.RosBridgeClient.JointStatePublisher::UpdateJointState(System.Int32)
extern void JointStatePublisher_UpdateJointState_m49D05DCFB8F5B1FA1E67249EDE76A54A0E50AE66 (void);
// 0x000000F8 System.Void RosSharp.RosBridgeClient.JointStatePublisher::.ctor()
extern void JointStatePublisher__ctor_m3A9993A4018A470961AD3E89901178677451E7DB (void);
// 0x000000F9 System.Void RosSharp.RosBridgeClient.JointStateSubscriber::ReceiveMessage(RosSharp.RosBridgeClient.MessageTypes.Sensor.JointState)
extern void JointStateSubscriber_ReceiveMessage_m086F869D3638D2D8BEC91EF65AE3A5A5E3F9447F (void);
// 0x000000FA System.Void RosSharp.RosBridgeClient.JointStateSubscriber::.ctor()
extern void JointStateSubscriber__ctor_m362303B05D4AB7C553F6BD720EDDE8AAB3765E3E (void);
// 0x000000FB System.Void RosSharp.RosBridgeClient.JoyPublisher::Start()
extern void JoyPublisher_Start_mD20F8147EB1EFE683841C01F78E171DD25097C2B (void);
// 0x000000FC System.Void RosSharp.RosBridgeClient.JoyPublisher::Update()
extern void JoyPublisher_Update_mC5104CF10964B2EF879FB83BA61A8AE42CCA4D09 (void);
// 0x000000FD System.Void RosSharp.RosBridgeClient.JoyPublisher::InitializeGameObject()
extern void JoyPublisher_InitializeGameObject_mFCE117F7916E76FD5FDFD55F743FBFCC2D37E5C7 (void);
// 0x000000FE System.Void RosSharp.RosBridgeClient.JoyPublisher::InitializeMessage()
extern void JoyPublisher_InitializeMessage_m595D02A82EB51675111CAFFC5C7B9AEF1C0E20DF (void);
// 0x000000FF System.Void RosSharp.RosBridgeClient.JoyPublisher::UpdateMessage()
extern void JoyPublisher_UpdateMessage_m4C5D6E44A6718D4327727AFE0F8AC414D28C463D (void);
// 0x00000100 System.Void RosSharp.RosBridgeClient.JoyPublisher::.ctor()
extern void JoyPublisher__ctor_m537C3685BB4F780B0DF990DD4C5433B3E548901A (void);
// 0x00000101 System.Void RosSharp.RosBridgeClient.JoySubscriber::Start()
extern void JoySubscriber_Start_mCE870A3CD4751CC9E68B898092D917B42424FB80 (void);
// 0x00000102 System.Void RosSharp.RosBridgeClient.JoySubscriber::ReceiveMessage(RosSharp.RosBridgeClient.MessageTypes.Sensor.Joy)
extern void JoySubscriber_ReceiveMessage_m47E2258B644484757FF54A2529810E50DBF1EEC3 (void);
// 0x00000103 System.Void RosSharp.RosBridgeClient.JoySubscriber::.ctor()
extern void JoySubscriber__ctor_m6D77066CEA7DF62D5A3F7E6B9DA17DB39C3DEB8F (void);
// 0x00000104 System.Void RosSharp.RosBridgeClient.LaserScanPublisher::Start()
extern void LaserScanPublisher_Start_m366551950DBFB199F46C56D3B67D140A01A6972C (void);
// 0x00000105 System.Void RosSharp.RosBridgeClient.LaserScanPublisher::FixedUpdate()
extern void LaserScanPublisher_FixedUpdate_m736CF9DB1866A578FBB0B36A8918A9B574A2B266 (void);
// 0x00000106 System.Void RosSharp.RosBridgeClient.LaserScanPublisher::InitializeMessage()
extern void LaserScanPublisher_InitializeMessage_mF79ADEB9614A1DA7F5EF42272305417200BCB56F (void);
// 0x00000107 System.Void RosSharp.RosBridgeClient.LaserScanPublisher::UpdateMessage()
extern void LaserScanPublisher_UpdateMessage_mD9FDEC6D402BF9504FD1C2A40429DBD8E204CABC (void);
// 0x00000108 System.Void RosSharp.RosBridgeClient.LaserScanPublisher::.ctor()
extern void LaserScanPublisher__ctor_m853590B41D6C19A8FA91933AF7D7B2C66D871CB0 (void);
// 0x00000109 System.Void RosSharp.RosBridgeClient.LaserScanSubscriber::Start()
extern void LaserScanSubscriber_Start_mD6EE9F911AAFC6209EB68D938B2E847D77E91AA3 (void);
// 0x0000010A System.Void RosSharp.RosBridgeClient.LaserScanSubscriber::ReceiveMessage(RosSharp.RosBridgeClient.MessageTypes.Sensor.LaserScan)
extern void LaserScanSubscriber_ReceiveMessage_m899CB9C68C15D9458C093FD2D1AFEBE1ADB77743 (void);
// 0x0000010B System.Void RosSharp.RosBridgeClient.LaserScanSubscriber::.ctor()
extern void LaserScanSubscriber__ctor_m1AE15B436B1052FFFAAC553A12999C80B238C5FE (void);
// 0x0000010C System.Void RosSharp.RosBridgeClient.OdometrySubscriber::Start()
extern void OdometrySubscriber_Start_m57B0F4438AE55C2D1919F9993DE41296A031845C (void);
// 0x0000010D System.Void RosSharp.RosBridgeClient.OdometrySubscriber::Update()
extern void OdometrySubscriber_Update_m2DAD3ECF2AB47D6342C734636CA607F71BB0A120 (void);
// 0x0000010E System.Void RosSharp.RosBridgeClient.OdometrySubscriber::ReceiveMessage(RosSharp.RosBridgeClient.MessageTypes.Nav.Odometry)
extern void OdometrySubscriber_ReceiveMessage_mA1FEE5C5EF20764E610EE015FF971046C91130A6 (void);
// 0x0000010F System.Void RosSharp.RosBridgeClient.OdometrySubscriber::ProcessMessage()
extern void OdometrySubscriber_ProcessMessage_mC69971D7FC944961492E23EE8E6AFE2B1A0A6F9E (void);
// 0x00000110 UnityEngine.Vector3 RosSharp.RosBridgeClient.OdometrySubscriber::GetPosition(RosSharp.RosBridgeClient.MessageTypes.Nav.Odometry)
extern void OdometrySubscriber_GetPosition_mB09EB6668660424D92C1327C60D9CE4BA03BBAD5 (void);
// 0x00000111 UnityEngine.Quaternion RosSharp.RosBridgeClient.OdometrySubscriber::GetRotation(RosSharp.RosBridgeClient.MessageTypes.Nav.Odometry)
extern void OdometrySubscriber_GetRotation_m7D23E7E87212642FCD5D7E12B2F4E56A1E3D3F17 (void);
// 0x00000112 System.Void RosSharp.RosBridgeClient.OdometrySubscriber::.ctor()
extern void OdometrySubscriber__ctor_m1A2D7A442AC73E84A8EBE293BA62292698583856 (void);
// 0x00000113 System.Void RosSharp.RosBridgeClient.PoseStampedPublisher::Start()
extern void PoseStampedPublisher_Start_m306D99490083CF75E9639CFB15AA777ABDB68238 (void);
// 0x00000114 System.Void RosSharp.RosBridgeClient.PoseStampedPublisher::FixedUpdate()
extern void PoseStampedPublisher_FixedUpdate_m5802F25366565F526D4682705CE4A99F5BF66F7D (void);
// 0x00000115 System.Void RosSharp.RosBridgeClient.PoseStampedPublisher::InitializeMessage()
extern void PoseStampedPublisher_InitializeMessage_m27D97CF13250BB6029CC1F812C274BAF379CF44D (void);
// 0x00000116 System.Void RosSharp.RosBridgeClient.PoseStampedPublisher::UpdateMessage()
extern void PoseStampedPublisher_UpdateMessage_m17DC255DF0553720F03348E0A13091FBD72AD2DA (void);
// 0x00000117 System.Void RosSharp.RosBridgeClient.PoseStampedPublisher::GetGeometryPoint(UnityEngine.Vector3,RosSharp.RosBridgeClient.MessageTypes.Geometry.Point)
extern void PoseStampedPublisher_GetGeometryPoint_m5AC7EEA2CAACA619A7BAA32A35FD2667BAAB4509 (void);
// 0x00000118 System.Void RosSharp.RosBridgeClient.PoseStampedPublisher::GetGeometryQuaternion(UnityEngine.Quaternion,RosSharp.RosBridgeClient.MessageTypes.Geometry.Quaternion)
extern void PoseStampedPublisher_GetGeometryQuaternion_m3B31AE1BB5A605397CEF1D2701104BBA1BE62F9C (void);
// 0x00000119 System.Void RosSharp.RosBridgeClient.PoseStampedPublisher::.ctor()
extern void PoseStampedPublisher__ctor_m5CC34F0A93C7C66F3BD057B79EC1E0609D7BD874 (void);
// 0x0000011A System.Void RosSharp.RosBridgeClient.PoseStampedSubscriber::Start()
extern void PoseStampedSubscriber_Start_m85EAB7877C014A82F43CA7CD27D8F71508142EE0 (void);
// 0x0000011B System.Void RosSharp.RosBridgeClient.PoseStampedSubscriber::Update()
extern void PoseStampedSubscriber_Update_mC5C081CB7616AF23D81F7AE6F312BA4CD3B2F6AC (void);
// 0x0000011C System.Void RosSharp.RosBridgeClient.PoseStampedSubscriber::ReceiveMessage(RosSharp.RosBridgeClient.MessageTypes.Geometry.PoseStamped)
extern void PoseStampedSubscriber_ReceiveMessage_m188037AE972B413E9C2318CF05161FE0332973C9 (void);
// 0x0000011D System.Void RosSharp.RosBridgeClient.PoseStampedSubscriber::ProcessMessage()
extern void PoseStampedSubscriber_ProcessMessage_m7F111F5AC23B48BF22B134181F07EFB435DD7C7E (void);
// 0x0000011E UnityEngine.Vector3 RosSharp.RosBridgeClient.PoseStampedSubscriber::GetPosition(RosSharp.RosBridgeClient.MessageTypes.Geometry.PoseStamped)
extern void PoseStampedSubscriber_GetPosition_m7C99B357E3D4F249E00F958443DB1A0411C250EB (void);
// 0x0000011F UnityEngine.Quaternion RosSharp.RosBridgeClient.PoseStampedSubscriber::GetRotation(RosSharp.RosBridgeClient.MessageTypes.Geometry.PoseStamped)
extern void PoseStampedSubscriber_GetRotation_m9235DC8387A86D00CFD1C2BA44BFD267D4E06862 (void);
// 0x00000120 System.Void RosSharp.RosBridgeClient.PoseStampedSubscriber::.ctor()
extern void PoseStampedSubscriber__ctor_mC9F379080F6154169E7A399E3FF9EE53B806ED14 (void);
// 0x00000121 RosSharp.RosBridgeClient.RosSocket RosSharp.RosBridgeClient.RosConnector::get_RosSocket()
extern void RosConnector_get_RosSocket_m26BD99A690D5BAF9C2FDD19AA34F424814787CCF (void);
// 0x00000122 System.Void RosSharp.RosBridgeClient.RosConnector::set_RosSocket(RosSharp.RosBridgeClient.RosSocket)
extern void RosConnector_set_RosSocket_m51E668B3EB3AE10900C3C638CB87EEC62E2DB345 (void);
// 0x00000123 System.Threading.ManualResetEvent RosSharp.RosBridgeClient.RosConnector::get_IsConnected()
extern void RosConnector_get_IsConnected_mD887BDF52CC256253976C87BD83EF0E00F90ED56 (void);
// 0x00000124 System.Void RosSharp.RosBridgeClient.RosConnector::set_IsConnected(System.Threading.ManualResetEvent)
extern void RosConnector_set_IsConnected_m6C2CF420228F86A69B2FAEAB15BBD7D633942D1C (void);
// 0x00000125 System.Void RosSharp.RosBridgeClient.RosConnector::Awake()
extern void RosConnector_Awake_m11893337639AEDF08AB07F63F6141768F7C002BE (void);
// 0x00000126 System.Void RosSharp.RosBridgeClient.RosConnector::ConnectAndWait()
extern void RosConnector_ConnectAndWait_m1EC4CF8AF4BF61AA53D97A90B8ADDC56252DB62C (void);
// 0x00000127 RosSharp.RosBridgeClient.RosSocket RosSharp.RosBridgeClient.RosConnector::ConnectToRos(RosSharp.RosBridgeClient.Protocols.Protocol,System.String,System.EventHandler,System.EventHandler,RosSharp.RosBridgeClient.RosSocket/SerializerEnum)
extern void RosConnector_ConnectToRos_m73C3ADC10993DFE93B1E4D48CF217D3301D81FEB (void);
// 0x00000128 RosSharp.RosBridgeClient.Protocols.IProtocol RosSharp.RosBridgeClient.RosConnector::GetProtocol(RosSharp.RosBridgeClient.Protocols.Protocol,System.String)
extern void RosConnector_GetProtocol_m5493BA0951116937B868C37B4810153F95668142 (void);
// 0x00000129 System.Void RosSharp.RosBridgeClient.RosConnector::OnApplicationQuit()
extern void RosConnector_OnApplicationQuit_mE3FDF1BCE3FD48B30B95AD8397FDAE31C81BE874 (void);
// 0x0000012A System.Void RosSharp.RosBridgeClient.RosConnector::OnConnected(System.Object,System.EventArgs)
extern void RosConnector_OnConnected_m99790D627FCC2E4128DE6B58071F58262641083D (void);
// 0x0000012B System.Void RosSharp.RosBridgeClient.RosConnector::OnClosed(System.Object,System.EventArgs)
extern void RosConnector_OnClosed_m96CC3481B1810ECD8727D6C4B32A4FE7AFE2AF20 (void);
// 0x0000012C System.Void RosSharp.RosBridgeClient.RosConnector::.ctor()
extern void RosConnector__ctor_m2A6E4432B5754A91BF99FA4EA96B6CDDE18E4EE0 (void);
// 0x0000012D System.Void RosSharp.RosBridgeClient.TwistPublisher::Start()
extern void TwistPublisher_Start_mC3CB262522036C1BAD8B70664F7FC83C4CED9476 (void);
// 0x0000012E System.Void RosSharp.RosBridgeClient.TwistPublisher::FixedUpdate()
extern void TwistPublisher_FixedUpdate_m8CFF85AA29399829F28CB0DA7FD961027C2635DA (void);
// 0x0000012F System.Void RosSharp.RosBridgeClient.TwistPublisher::InitializeMessage()
extern void TwistPublisher_InitializeMessage_m1AE1ED14C80E4E1CDA3D16D56D3FC2DB9F4462A6 (void);
// 0x00000130 System.Void RosSharp.RosBridgeClient.TwistPublisher::UpdateMessage()
extern void TwistPublisher_UpdateMessage_m79F4E16AA7A2BD1BADD5FA427EC01A9F1640421E (void);
// 0x00000131 RosSharp.RosBridgeClient.MessageTypes.Geometry.Vector3 RosSharp.RosBridgeClient.TwistPublisher::GetGeometryVector3(UnityEngine.Vector3)
extern void TwistPublisher_GetGeometryVector3_m280A75814E489E78D750A9DFFB84CCC7F615E48C (void);
// 0x00000132 System.Void RosSharp.RosBridgeClient.TwistPublisher::.ctor()
extern void TwistPublisher__ctor_m595A5C6EDBC36987BC4A88194577281AE4C1CAC2 (void);
// 0x00000133 System.Void RosSharp.RosBridgeClient.TwistSubscriber::Start()
extern void TwistSubscriber_Start_mFAE0B0CCD7954FC40F2701FD025A13E9453C55C3 (void);
// 0x00000134 System.Void RosSharp.RosBridgeClient.TwistSubscriber::ReceiveMessage(RosSharp.RosBridgeClient.MessageTypes.Geometry.Twist)
extern void TwistSubscriber_ReceiveMessage_mCD49E95A982BEDB39C5C7C20E087911FBBEF89FA (void);
// 0x00000135 UnityEngine.Vector3 RosSharp.RosBridgeClient.TwistSubscriber::ToVector3(RosSharp.RosBridgeClient.MessageTypes.Geometry.Vector3)
extern void TwistSubscriber_ToVector3_mF32BD3534F3B2DA5E37D39592C118951858F632F (void);
// 0x00000136 System.Void RosSharp.RosBridgeClient.TwistSubscriber::Update()
extern void TwistSubscriber_Update_m41B43FDEE75A35B0E7113E386F9C1D2B4E230D99 (void);
// 0x00000137 System.Void RosSharp.RosBridgeClient.TwistSubscriber::ProcessMessage()
extern void TwistSubscriber_ProcessMessage_m2CC679AAC66AB38F4E77AC011B1134CD695F0F48 (void);
// 0x00000138 System.Void RosSharp.RosBridgeClient.TwistSubscriber::.ctor()
extern void TwistSubscriber__ctor_m97903704E8A5BC94B661043FC205334E1F4DC7CF (void);
// 0x00000139 System.Void RosSharp.RosBridgeClient.UnityPublisher`1::Start()
// 0x0000013A System.Void RosSharp.RosBridgeClient.UnityPublisher`1::Publish(T)
// 0x0000013B System.Void RosSharp.RosBridgeClient.UnityPublisher`1::.ctor()
// 0x0000013C System.Void RosSharp.RosBridgeClient.UnityServiceProvider`2::Start()
// 0x0000013D System.Boolean RosSharp.RosBridgeClient.UnityServiceProvider`2::ServiceCallHandler(Tin,Tout&)
// 0x0000013E System.Void RosSharp.RosBridgeClient.UnityServiceProvider`2::.ctor()
// 0x0000013F System.Void RosSharp.RosBridgeClient.UnitySubscriber`1::Start()
// 0x00000140 System.Void RosSharp.RosBridgeClient.UnitySubscriber`1::Subscribe()
// 0x00000141 System.Void RosSharp.RosBridgeClient.UnitySubscriber`1::ReceiveMessage(T)
// 0x00000142 System.Void RosSharp.RosBridgeClient.UnitySubscriber`1::.ctor()
// 0x00000143 System.Void RosSharp.RosBridgeClient.LaserScanVisualizer::Visualize()
// 0x00000144 System.Void RosSharp.RosBridgeClient.LaserScanVisualizer::DestroyObjects()
// 0x00000145 System.Void RosSharp.RosBridgeClient.LaserScanVisualizer::Update()
extern void LaserScanVisualizer_Update_m8BA6863E933DDC31CC226E80A3153AE3AB5718EC (void);
// 0x00000146 System.Void RosSharp.RosBridgeClient.LaserScanVisualizer::OnDisable()
extern void LaserScanVisualizer_OnDisable_mE135111F72D59A25F6EC8351F4305AF6DEC60057 (void);
// 0x00000147 System.Void RosSharp.RosBridgeClient.LaserScanVisualizer::SetSensorData(UnityEngine.Transform,UnityEngine.Vector3[],System.Single[],System.Single,System.Single)
extern void LaserScanVisualizer_SetSensorData_mAB4FC5A5C0175AABEEF47A03E27C71ECD17CF2A6 (void);
// 0x00000148 UnityEngine.Color RosSharp.RosBridgeClient.LaserScanVisualizer::GetColor(System.Single)
extern void LaserScanVisualizer_GetColor_mFD8FB9975D4B509FACB3D0D52246981684EBD1D8 (void);
// 0x00000149 System.Void RosSharp.RosBridgeClient.LaserScanVisualizer::.ctor()
extern void LaserScanVisualizer__ctor_m8F96510A95D0883091471EE4E3CC9D404975359C (void);
// 0x0000014A System.Void RosSharp.RosBridgeClient.LaserScanVisualizerLines::Create(System.Int32)
extern void LaserScanVisualizerLines_Create_m671E478127AC68844D1D75D89AC633AC7A6012FD (void);
// 0x0000014B System.Void RosSharp.RosBridgeClient.LaserScanVisualizerLines::Visualize()
extern void LaserScanVisualizerLines_Visualize_m93F0AE35BA03095F7FB9246DEDBFCB9D003A0770 (void);
// 0x0000014C System.Void RosSharp.RosBridgeClient.LaserScanVisualizerLines::DestroyObjects()
extern void LaserScanVisualizerLines_DestroyObjects_m2C041C6F1E565B1AE1E5893EC0F3C1911B933997 (void);
// 0x0000014D System.Void RosSharp.RosBridgeClient.LaserScanVisualizerLines::.ctor()
extern void LaserScanVisualizerLines__ctor_m0B9FF6E18820F2E08B321F56A05D3C14088BEAFA (void);
// 0x0000014E System.Void RosSharp.RosBridgeClient.LaserScanVisualizerMesh::Create()
extern void LaserScanVisualizerMesh_Create_m30A0FB87369307FFD324EF2CD997CA8436069509 (void);
// 0x0000014F System.Void RosSharp.RosBridgeClient.LaserScanVisualizerMesh::Visualize()
extern void LaserScanVisualizerMesh_Visualize_m1C7A702A745070A7FC29F91920C136CD4DB43A16 (void);
// 0x00000150 System.Void RosSharp.RosBridgeClient.LaserScanVisualizerMesh::DestroyObjects()
extern void LaserScanVisualizerMesh_DestroyObjects_m175FBA7D4F1B2AA3DF6326C03FF54113945E0991 (void);
// 0x00000151 System.Void RosSharp.RosBridgeClient.LaserScanVisualizerMesh::.ctor()
extern void LaserScanVisualizerMesh__ctor_mD740705D50E55B5B5EBDADBC1C09E29405D5660E (void);
// 0x00000152 System.Void RosSharp.RosBridgeClient.LaserScanVisualizerSpheres::Create(System.Int32)
extern void LaserScanVisualizerSpheres_Create_m4D18B5A8C30F8458233FB11BF8AB093CD4EEC2B5 (void);
// 0x00000153 System.Void RosSharp.RosBridgeClient.LaserScanVisualizerSpheres::Visualize()
extern void LaserScanVisualizerSpheres_Visualize_mC1DE9D7817D5C3B57EB2FB2445E7CF92DDCE2A62 (void);
// 0x00000154 System.Void RosSharp.RosBridgeClient.LaserScanVisualizerSpheres::DestroyObjects()
extern void LaserScanVisualizerSpheres_DestroyObjects_m0E0913B4AA3E040441984A9B23A4B3E8723FD5F1 (void);
// 0x00000155 System.Void RosSharp.RosBridgeClient.LaserScanVisualizerSpheres::.ctor()
extern void LaserScanVisualizerSpheres__ctor_mAE41CAF64198208F1D10B4AC1F894FEBAC427B02 (void);
// 0x00000156 RosSharp.RosBridgeClient.MessageTypes.Std.Time RosSharp.RosBridgeClient.Timer::Now()
extern void Timer_Now_m5C42BB8E9874B2A27C5CD610895811D2E1403A5C (void);
// 0x00000157 System.Void RosSharp.RosBridgeClient.Timer::Now(RosSharp.RosBridgeClient.MessageTypes.Std.Time)
extern void Timer_Now_m436BED554FB77406699EE5D4837E13C859F9FF9E (void);
// 0x00000158 System.Void RosSharp.RosBridgeClient.Timer::Now(System.UInt32&,System.UInt32&)
extern void Timer_Now_m95C982DCD42DCF8EBABAF66301E91A6B78DE1E18 (void);
// 0x00000159 System.Void RosSharp.RosBridgeClient.Timer::.ctor()
extern void Timer__ctor_m97466C6A4381C1535FDD8E013DF4376C3983E199 (void);
// 0x0000015A System.Void RosSharp.RosBridgeClient.Timer::.cctor()
extern void Timer__cctor_m2D2910DF419FB0DCFEFD340336AD760750A91CFF (void);
// 0x0000015B System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionClient::Start()
extern void UnityFibonacciActionClient_Start_mE516BD32AE216A8768EEB3AB944C55B3B08CC31B (void);
// 0x0000015C System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionClient::Update()
extern void UnityFibonacciActionClient_Update_m4E8C9C72A867AF1B50428E40EA62352689AD2319 (void);
// 0x0000015D System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionClient::RegisterGoal()
extern void UnityFibonacciActionClient_RegisterGoal_mA88B5024EC5AE582261A1B1334F4AF29BA8E9511 (void);
// 0x0000015E System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionClient::.ctor()
extern void UnityFibonacciActionClient__ctor_mC1BDC0B71922FEE7880D5A1364B43EE6C0BAF67F (void);
// 0x0000015F System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionSever::Start()
extern void UnityFibonacciActionSever_Start_m93E6FA8FD95397FC447ED989CFECFDB1167432EA (void);
// 0x00000160 System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionSever::Update()
extern void UnityFibonacciActionSever_Update_mA57398617B3915BCEAF687E40D0426DCB3B727B0 (void);
// 0x00000161 System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionSever::.ctor()
extern void UnityFibonacciActionSever__ctor_m8301330C5AC047490A9B31DB39496E89C395FEFC (void);
// 0x00000162 System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionSever/<>c::.cctor()
extern void U3CU3Ec__cctor_mE5D4E1597C71D0C3E04B77271A45E68550CEEB8B (void);
// 0x00000163 System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionSever/<>c::.ctor()
extern void U3CU3Ec__ctor_m5B0E1DED4BDFEF400D2768BE929BAEFE44A7B27B (void);
// 0x00000164 System.Void RosSharp.RosBridgeClient.Actionlib.UnityFibonacciActionSever/<>c::<Start>b__5_0(System.String)
extern void U3CU3Ec_U3CStartU3Eb__5_0_m7AE32C9CE21D8F0252797845F7CB6CDAB40F44CB (void);
static Il2CppMethodPointer s_methodPointers[356] = 
{
	NULL,
	NULL,
	TransformExtensions_SetParentAndAlign_m3777C3513290D7C618F369C34EECDB1750BB1AE0,
	TransformExtensions_HasExactlyOneChild_m17FDA8B3ABC5E2F943CACB9D8BF6E82772AE1846,
	TransformExtensions_MoveChildTransformToParent_m281BF82F78DF9DEDF57E6C1D800BC065049E5230,
	TransformExtensions_Ros2Unity_m11467E18A90D469732140E3C3EB11904393C06FE,
	TransformExtensions_Unity2Ros_m74703B5E4DEFB516AB071300101C988D494E52E1,
	TransformExtensions_Ros2UnityScale_m33150967D5F4DA5AE28E1F33E2792DC71460B3CA,
	TransformExtensions_Unity2RosScale_m2C3C5965D35797490D2D77DD8E3DDC325166D55E,
	TransformExtensions_Ros2Unity_m9A91003341D53EAEE8EC18EFE3353F213E717728,
	TransformExtensions_Unity2Ros_m0A420C0A2D4862981403680087FC5CB19E90558D,
	TransformExtensions_ToRoundedDoubleArray_m6C4AD90B183308C3756479E95F2C5A061F043C6A,
	TransformExtensions_ToVector3_mFA2848545680A5B674DEBCDF26AF41114E838672,
	TransformExtensions_SetSeparatorChar_mC6095D5815B819F17C287ADB6BD16EFCDE3E1498,
	HingeJointLimitsManager_get_AngleActual_m78E09DBCC192148981559333426CEBFCA80C397D,
	HingeJointLimitsManager_set_AngleActual_m9BB4978C9FF2C6058CABFC4462E2B0DC0F57F2C8,
	HingeJointLimitsManager_get_RotationNumberActual_m7B068097B0F522D9F03F1A9D850B3B76FC7150C6,
	HingeJointLimitsManager_set_RotationNumberActual_m3F41CEE79E28D2C53CB6CA7BC523AE0F3AF472FF,
	HingeJointLimitsManager_get_RotationNumberMin_m81F5417EE86CD5F570B33CD8CF003CE1F2157C7F,
	HingeJointLimitsManager_set_RotationNumberMin_mE5F456566B3A01F4C5F2B223623C415A4388C204,
	HingeJointLimitsManager_get_RotationNumberMax_m293FB24FD097B90CC9F115997409F3AAA1AB56EA,
	HingeJointLimitsManager_set_RotationNumberMax_mB7BC17C415248A9CBC19C270EEE5F3CB323EB0D5,
	HingeJointLimitsManager_get_AngleLimitMin_m3764FD62F95C4FEE3C779F783CACFD2AB21ADD5F,
	HingeJointLimitsManager_set_AngleLimitMin_mB88421F9F14F2BA476E7D5F0FA5AC1B2F19AAC6C,
	HingeJointLimitsManager_get_AngleLimitMax_m8CB0D26BF2922509731C4E7403730A0B9C678A02,
	HingeJointLimitsManager_set_AngleLimitMax_m7AD295FD06810FF2727B882182974E476D7F79A2,
	HingeJointLimitsManager_Awake_m081555A1647E57CC3C89A413CDB265E98C484163,
	HingeJointLimitsManager_Update_mAF5284F59611F61DC202C9CA459617A085A1F7E2,
	HingeJointLimitsManager_FixedUpdate_m9B14D977F51D3AD9ACED9F374C3A91FA1B3F2B4A,
	HingeJointLimitsManager_RecalculateJointLimits_mA2B534DCA05835B60CCD76235EABDCA9A040D91C,
	HingeJointLimitsManager_FixAngleLimits_mF2DF41ED7052D6FC223114726292A4A3D5F6C8BB,
	HingeJointLimitsManager_GetAngleLimit_m5DC0FD456E0AE5EEC707C8865580AFA936CF5C07,
	HingeJointLimitsManager_GetRotationLimit_m8437873DBDEC77BC9FF99C4C7C1C7D0363B68B74,
	HingeJointLimitsManager_UpdateAngles_mCC71C15933C9797767519FE1CA7902F0FC9A6F26,
	HingeJointLimitsManager_ApplyJointLimits_m0E7DC9996E308CCB09EED2E0B279E69637124DBF,
	HingeJointLimitsManager_UpdateLimits_mAA5708CA0843DB903C11D1155D8A29F34DD30720,
	HingeJointLimitsManager_InitializeLimits_m17A712C8BBC2274E58ED930366F9101E27F680D3,
	HingeJointLimitsManager__ctor_m0B100866C1C5167DDF2AD8E8D734DBB9CDFF1C16,
	PrismaticJointLimitsManager_Awake_m0D2113332557B71292D59090A63478D09F40DE34,
	PrismaticJointLimitsManager_FixedUpdate_m1ED0DC0653ED7B4B6D328D29901925A8ED3C43E9,
	PrismaticJointLimitsManager_OnValidate_mF69C06A13B2D2105C3C164226DCF5098981A4329,
	PrismaticJointLimitsManager_ApplyLimits_m71EE903FA157F2041B34D8865744E79BAE6CA1BA,
	PrismaticJointLimitsManager_UpdateLimit_m6DD4ADEFD225A1D5F0FC6B3347C54A56AC9B3966,
	PrismaticJointLimitsManager_InitializeLimits_mFBC54B956CB1B66C8C68FEB61E620B72EDC9C529,
	PrismaticJointLimitsManager__ctor_mD4C7C18133BAF3B24BDF43F69540359FDFDA48EE,
	Matrix3x3__ctor_mE0DE3B45AB53FDA2326CFE93F6BAD79346763C08,
	Matrix3x3__ctor_m6B9C729CF734F0C21CAD9FF1152748F33332019B,
	Matrix3x3__ctor_m27F9291C3D3C0A68F8A017AB16CDE75ED600FFD0,
	Matrix3x3__ctor_mF8613E46AC17A5DBE4D4BF9472EB232E85A5BFAB,
	Matrix3x3_get_Item_m610B8C72B0C88596013D1A2912FD8E805EC7A96D,
	Matrix3x3_set_Item_m8A1441B7DCC830641573E07E5043C973F8583F22,
	Matrix3x3_op_Addition_m8DFB580605C0011CC3DA7D8480C62B3AA7A884DE,
	Matrix3x3_op_Addition_mE5AB6CF747CCB4F38AE179BC10033DDBCF3AC9D1,
	Matrix3x3_op_Subtraction_mE9656C50B713E16317F3AD3A0C7D0FE32B272833,
	Matrix3x3_op_Subtraction_m9B055CD0B0F3F0B56648581DDF1D2175E7247DE1,
	Matrix3x3_op_Multiply_m0DE9707A47A82FCE110704BE18AEE3480EC755B1,
	Matrix3x3_op_Multiply_mF9C33D4AB925BEB9B7A0BC60DACAF205E901646D,
	Matrix3x3_op_Multiply_mF7A82855527329FACE92509EFEE6AE14642C9C49,
	Matrix3x3_op_Multiply_m3C05F5EAD02795A7D215696C8D28BAA0A436223B,
	Matrix3x3_VectorMult_mFFCEBB2C3875E892653A25729143A58690BAA4E9,
	Matrix3x3_Determinant_m7BD915468295A8E1AAF6800D3FB8CD4565396C7D,
	Matrix3x3_Trace_mD8CEEBAB1F09366D867FBC679B568E727ED7B70B,
	Matrix3x3_IsDiagonal_mBD4AEC38A96F1BA83FE6F13C1A27DD520B515386,
	Matrix3x3_Transpose_m8B922F09ADC03C719BBC879D090F72DBDBCE0E17,
	Matrix3x3_DiagonalizeRealSymmetric_m61171FCB0F832068D9C97BD0DD8C2FFD65BFC499,
	Matrix3x3_Eigenvalues_m51F3BE4FC3B8C06BD760AF8F7232677F089ACB60,
	Matrix3x3_Eigenvectors_m2DA9DAE008CDED046767058D03479323A8D65B93,
	Matrix3x3_GetEigenvector0_m269C859F31F2F3146488E959AE25940A9CE25308,
	Matrix3x3_GetEigenvector1_m3EEC60FC4C4ADB06527BAF1B23449D820D518A57,
	Matrix3x3_GetEigenvector2_mB33265DB21B6EAD34C8526097A5A7C74306E880C,
	Matrix3x3_CalculateOrthogonalComplement_m83AD44186B0AB721FF34C824B0A7FD1E4CF9F3F0,
	Matrix3x3_IsTwoEigenvaluesEqual_m806ED5AD5B0423D4E8911D9E5D4A77C9064031B2,
	UrdfCollision__ctor_m7793B962C349230D061ACCEC029BF4CE397DBBBD,
	UrdfCollisions__ctor_m53AAD614EA26097FCD16C559F23B7B13875A647D,
	UrdfInertial_Create_mD9FF1702FC81F395C3424B1DF27081E0E3D96DAE,
	UrdfInertial_Initialize_mB2916A97B02CB3997BA74AAA95E27CFBE1261C2E,
	UrdfInertial_Reset_m25EA58756731DD6740CC880901C136DC5E7C065A,
	UrdfInertial_OnValidate_mBBD257430D2FE8FDA8F428EF71AECCE0013100DB,
	UrdfInertial_UpdateRigidBodyData_m6A5BD6A7186551CC741C18B8DADA83D26C8FEB7A,
	UrdfInertial_OnDrawGizmosSelected_m254F8AFE534D67EE1B3B22BE1AAF423E5E2264B0,
	UrdfInertial_ImportInertiaData_mFF7DFCD0E7C8D3CADEC01B173CEBB0BFE903A71F,
	UrdfInertial_ToUnityInertiaTensor_mF73ED748D0F4FE156FAABCDA9C072625161EB587,
	UrdfInertial_ToMatrix3x3_mD43846549283D076FBFDC60E28D89391284C9014,
	UrdfInertial_FixMinInertia_mF5EF4ED228EECFDA7D4381BACF50C4A6CB3D31F7,
	UrdfInertial_ToQuaternion_m5D5500ABC7A74024F0D424BF44F500BDEF197EDE,
	UrdfInertial_ExportInertialData_m89A5380F51A33887CBCD7AB73E77A40396DDDA2E,
	UrdfInertial_ExportInertiaData_m81A547844C2824B8F55B072B348A0C7B75FE187C,
	UrdfInertial_Quaternion2Matrix_mD487E28D9A95B7762BF1DE22876C439BBBCFBB79,
	UrdfInertial_ToInertia_mE6BA124DED04AAB8CBBCF85A4278A404CFD2FE29,
	UrdfInertial_ToRosCoordinates_m57743F1012088C1A48D8F88B1BB13C4F38D3C38A,
	UrdfInertial__ctor_m1764ACEB6E57315211DB70DDC242E11691C1F3D2,
	UrdfJoint_get_JointType_m1C966143B9AC8783D9CF42A89B852C651ED8AECD,
	UrdfJoint_set_JointType_m8196165979DF2C4470456D96AE821A80FAD45D75,
	UrdfJoint_get_IsRevoluteOrContinuous_mD3B8E8C89951AD4C44E2C6BCC98206D6BFA8E012,
	UrdfJoint_Create_m8B715E80716C44A94609F5BCF897867D05774C2C,
	UrdfJoint_AddCorrectJointType_m4ED60BD9DEC372F4E1E5C401FC7926B50CF178DF,
	UrdfJoint_ChangeJointType_mFA8EF3A6B730D6C04296E9DA77B401A97109FB77,
	UrdfJoint_Start_m8EDF4E78607ABB306CCA2CB1BAC436C75AF3537A,
	UrdfJoint_GetPosition_mB2DC1C68EE96BC04B9AA0C103083D0C12B0B0328,
	UrdfJoint_GetVelocity_mFC42C346D0B9DE4E8F0D55885AFDA046FE8730F6,
	UrdfJoint_GetEffort_m7E51E92B83209920696ADB4972BF7F1F40BDB6B9,
	UrdfJoint_UpdateJointState_m9565C0AA4FF402C1BF47D58A73D0734F8C7C1D08,
	UrdfJoint_OnUpdateJointState_m71A88F42A534CEE00808963DEA7DC1AED3E3D478,
	UrdfJoint_GetJointType_mD37A3A094BF68F75E602D068EADA9FAF182F7668,
	UrdfJoint_ImportJointData_m8DD46CB18C937D8F9F2381CAF7ABA7DC18E2CC5B,
	UrdfJoint_GetAxis_m377B2DD77DB716FFB6C810B50F8010CFB75157DE,
	UrdfJoint_GetDefaultAxis_mC15D10371B3BA2E4BBCBD3A1A07F3663CB49E9B1,
	UrdfJoint_GetJointDrive_m7B41BF11296F4C80374037E7EDB2B80D7A09EF28,
	UrdfJoint_GetJointSpring_mD8B0BD896978B37D2FCF6525348337EEA95AD38C,
	UrdfJoint_GetLinearLimit_m606F9CCA7E468E69D49141C455D719FDE47E3F12,
	UrdfJoint_ExportJointData_m272A5C87509D955F7E39B716A800430589919F77,
	UrdfJoint_ExportDefaultJoint_mD163D9E86B5721B2439C724081A46DCD47E26C95,
	UrdfJoint_ExportSpecificJointData_m1AE373ABFED1C36A50123AC59669BF435233C6A2,
	UrdfJoint_ExportLimitData_m2E38139DEF3B52D54A1FB05FE7671E74F0F8EAF9,
	UrdfJoint_AreLimitsCorrect_m5655EA1722FCF9F613C2021D3A65B0AB4E136E66,
	UrdfJoint_IsJointAxisDefined_m3C165ACEE4CDF7E95438F2B1112D9EFD7FCA9444,
	UrdfJoint_GenerateUniqueJointName_m5D518038028AA0CA8E0BDF001F45C1C6605BE0F6,
	UrdfJoint_GetAxisData_mEE13E04B9C01177B927D624E0480ED064F418074,
	UrdfJoint_IsAnchorTransformed_m1ED8D68348C8D83545597E3395C2A16ABF6301B7,
	UrdfJoint_CheckForUrdfCompatibility_m3681EDA6606145ED2382047BEC84A8D9F7E28180,
	UrdfJoint__ctor_m207B2E8311C17443DC01FF33EC8227FCC9202D48,
	UrdfJointContinuous_Create_mE1C39B3DFE030EB4FE216E6AE54970DC3F120B46,
	UrdfJointContinuous_GetPosition_m656A4200647E6F18CB72A1E9A5C66BA0BB0A8AAF,
	UrdfJointContinuous_GetVelocity_mD66D957E869D42E5E5DBC33E85520437A54C5404,
	UrdfJointContinuous_GetEffort_m56D7DB7FBD4653BDD28348BC9B7B9A49BF5EB2E4,
	UrdfJointContinuous_OnUpdateJointState_m4B7F7285CDE156AD1175BE7E9D0E70DD6097FEA7,
	UrdfJointContinuous_ImportJointData_mE99C13F2A1BFA6ADF31EED8903CF68D26DA6F4E1,
	UrdfJointContinuous_ExportSpecificJointData_m38F806A4F50664119AD1D7CDC7132DFD9C8EC47F,
	UrdfJointContinuous__ctor_m9AF00FDF5791EECE4E52F33A915D0E5634CFE790,
	UrdfJointFixed_Create_m046048B5B65C299643769C19775AF2117F67C47A,
	UrdfJointFixed_IsJointAxisDefined_mBD6D6BBA26AA39DA470586A2ED0F2FC6F4029FDA,
	UrdfJointFixed__ctor_m42013A30E1426B02B814D88262EBF29F50411ABE,
	UrdfJointFloating_Create_mC659A9CB431AEC673FD93987975CF4C0EEA52B36,
	UrdfJointFloating_GetPosition_m3239CCF3094FBEF59AB0A587B8DFC84C6941C416,
	UrdfJointFloating_IsJointAxisDefined_m6B48E8462BA956E46B34B1CCDD452B7D5D7DF0E9,
	UrdfJointFloating__ctor_mE4E8E58678137F56E4CD53345E070E64516F4C69,
	UrdfJointPlanar_Create_m94E6C67AD61669B59101B93AE3FF17BE8C3B15BD,
	UrdfJointPlanar_GetPosition_m8B9537C0CD369F4A3E090420C6C45CA4495660A3,
	UrdfJointPlanar_ImportJointData_m38B69EBDDD033AD14ECE265E333D313F06374A3F,
	UrdfJointPlanar_ExportSpecificJointData_mDE08CC0B1E907F19CEC663CABDF0F37583D92433,
	UrdfJointPlanar_ExportLimitData_m2E97A443EE2982723333417AB2B65CC63D63E3D7,
	UrdfJointPlanar_AreLimitsCorrect_mCAD9785AA8C7CA1FCA19C7D5B63ED90CB81E9861,
	UrdfJointPlanar_IsJointAxisDefined_m060D14AFA5CF300A187A219C79DD956BAF804352,
	UrdfJointPlanar__ctor_m720D6F5E034E26B850207B1614BC733515473F42,
	UrdfJointPrismatic_Create_mFF2B6C608245FA9AD92C36AE38F24DFEE61CAD6D,
	UrdfJointPrismatic_GetPosition_mABB4772308FB32F4C5ECE5ABFE0223701C1A0D0B,
	UrdfJointPrismatic_OnUpdateJointState_m8D5848A53B1BB8EFB776A6F43BCB60853D0C5EBF,
	UrdfJointPrismatic_ImportJointData_m81EA5FD18D6639EFA6FF2EE99FAB723FADB68B27,
	UrdfJointPrismatic_ExportSpecificJointData_m0744FF9A3D68A6EC89877B1F01AFBB94CC570815,
	UrdfJointPrismatic_AreLimitsCorrect_mE5CB315B6809F7C4A56CF3C555E10CB5AE506DD1,
	UrdfJointPrismatic_ExportLimitData_m4AAC6A0F8A9B1236C814BACB136CC6ECC6CA0A61,
	UrdfJointPrismatic__ctor_m148DD4B601DC0BD3BF46EDE78BF751DD64A3F0CC,
	UrdfJointRevolute_Create_m4C963609227FB6653139A7CAD41DC627E786B46C,
	UrdfJointRevolute_GetPosition_mFE2B3F946181437081F1DCB1677C5B0D21CA3B48,
	UrdfJointRevolute_GetVelocity_m247827D372C2206AD62BFD1D909C9EEBAA64A139,
	UrdfJointRevolute_GetEffort_m91E5226824DB706554B59C1DEB6AB91C37EF8862,
	UrdfJointRevolute_OnUpdateJointState_m182D4514462BEC0A9D2EFFF8A28EB4CC9779AAAA,
	UrdfJointRevolute_ImportJointData_mD73C6F53C0208524F2E0250B76E9339A779AE0DA,
	UrdfJointRevolute_ExportSpecificJointData_m4420404CF9793A64DFB6554C9A17A7B8F34BA370,
	UrdfJointRevolute_AreLimitsCorrect_m21276E808A7F1ED5BCCC618989F32EF359FE6369,
	UrdfJointRevolute_ExportLimitData_m1E340BD34D16DA5E8000516A200C12961D1B1B8D,
	UrdfJointRevolute__ctor_m0AFD9DF73539DB8DAACCAA6E05C22605F724DC3D,
	UrdfLink__ctor_mDC1F5B4F00E1468FF62C2BB0CD3FB33A2D843CB6,
	UrdfOrigin_ImportOriginData_mEF583358C0F7F7D0444D62314B584F8D1A745DE7,
	UrdfOrigin_GetPositionFromUrdf_m77EF016FCEC3B2F9E5195A8E7F1E9426C8B008CA,
	UrdfOrigin_GetRotationFromUrdf_mC9F192CDFD432B762009EC855A2A4858F237870C,
	UrdfOrigin_ExportOriginData_mD1AB04662F1F7B7B91240E29542784E5D1C988EF,
	UrdfOrigin_ExportXyzData_m4C5BAE09BD06C1553527E3F0A6CD08B3BD4C6D1A,
	UrdfOrigin_ExportRpyData_mE753B5C0414F99DA1F7FEA742CDA0C6F9D7F43D8,
	UrdfPlugin_Create_mEF32FC7631BF4388C3A78E55ED5B4C6D32F7BC0E,
	UrdfPlugin_ExportPluginData_m0A59A4244303EFDD21BEC86C761447709478172F,
	UrdfPlugin__ctor_m339A815FD14C72D76749433AD4346A37818E7CBC,
	UrdfPlugins_Create_mC80611E2F177E45E64CC7E52E8AE297DB0C8A06B,
	UrdfPlugins_ExportPluginsData_m9CF32A0D0A2C700F8292417F6BF33A48E24AC048,
	UrdfPlugins__ctor_m89B9D44F4F5DBCF953D090BD552BB3E509221B96,
	U3CU3Ec__cctor_mC125EF0C21454C544D60F373535F88B353D8B895,
	U3CU3Ec__ctor_mDB61C170875ADF820A910BEE3239EC8C36A35CF9,
	U3CU3Ec_U3CExportPluginsDataU3Eb__1_0_mF8AD33592A69B12C9D6DD8D0A4A2F4410D084FC4,
	U3CU3Ec_U3CExportPluginsDataU3Eb__1_1_m51D2C8E264F4FAC62788169AB15EA8F131F17A95,
	UrdfRobot_SetCollidersConvex_mB196A7499C1CAF141935471A0D805DF7030C3A6E,
	UrdfRobot_SetRigidbodiesIsKinematic_mB700865E1A5A51973E38D972735A7625BE336FE7,
	UrdfRobot_SetUseUrdfInertiaData_mFF6EDD5542E13344E4E5614BC4B63AFA9BB58B64,
	UrdfRobot_SetRigidbodiesUseGravity_m9A7244771AB4D4D98E6279B9D886F451A354CC82,
	UrdfRobot_GenerateUniqueJointNames_m819F606B9561D1A21FE4E2BF63DDCF4C10C2DC08,
	UrdfRobot__ctor_mE18E0295E8CBF9F475F04EA3009B780D4AC80C82,
	UrdfVisual__ctor_mC36EB6BE1DCFB070DD0F21BB89E3765DFA38819F,
	UrdfVisuals__ctor_m3E00361E983E49FB3AB6D98B406A8DDE822CBF6C,
	HeaderExtensions_Update_m4DEAC48EBEB22EBA4400060CD3977F7DC512FE6A,
	HeaderExtensions__cctor_m6BE4656E0F3DF4C46384232EC169164DC42FC1EB,
	JointStatePatcher_SetPublishJointStates_mABDA01758571F7C0DA12BBCA23B04BBC70B63325,
	JointStatePatcher_SetSubscribeJointStates_mF94EA205BAEE2C158A31506767E3BEE793E8958F,
	JointStatePatcher__ctor_m3A620B25FD1176D20F6F793EF86220A13DA6340E,
	JointStateReader_Start_m6E340B7F91D61DAFD64E92954C51BEFE6B5EEBDA,
	JointStateReader_Read_m9CF7A1F1B029935A27471EB711E7FD85ECCF3E6D,
	JointStateReader__ctor_mDDFF4F94344AADA1B953EF1D014A08194B8CED21,
	JointStateWriter_Start_m2DAC1165DF4632CAF1E81E5AC0D91CF3EB98F753,
	JointStateWriter_Update_m0D57D2F9ECEA807957552BCD4F25762016A9C401,
	JointStateWriter_WriteUpdate_mAD78FE0A83A4D54739293CDADF909AA2C82E9826,
	JointStateWriter_Write_m2D9582D61B53E10F825BCAB2F836DC4631C20B72,
	JointStateWriter__ctor_m5391E355FFB1ADDE13DCC34208A1E91B171AB630,
	JoyAxisInputPasser_Start_mFA84A945B0B4F48197A9EB682267095BC3468459,
	JoyAxisInputPasser_Update_mEDCD98F51525BA0EA17408CF41D5A7179A12A815,
	JoyAxisInputPasser__ctor_m00954BF3F1091F11584AEB5D0CC9A5EA1F9CD14C,
	JoyAxisJointMotorWriter_Start_m7D973A248BA39897C47503C8D45C63091B0390F7,
	JoyAxisJointMotorWriter_Update_mEACDC13FCFD5B76FA1D332D30A761B3AFFE4262C,
	JoyAxisJointMotorWriter_ProcessMessage_mE019CB99A72988F88E90290D048F991DB53784A0,
	JoyAxisJointMotorWriter_Write_m7578AE3D748ECD6AB159D3EDE60ACE4F4118F205,
	JoyAxisJointMotorWriter__ctor_mAA66AE53DA17C352D29E359871151794B23E27EE,
	JoyAxisJointTransformWriter_Start_m87013FE3CE6F6470FB84D630EB23B7BAD8FFF3D9,
	JoyAxisJointTransformWriter_ApplyLimits_m6066DAFB054ABAC0931C4892FD5C1615BA066F08,
	JoyAxisJointTransformWriter_Update_mB5254B1FDA3399C4EAC68EEA38DD90BD15358942,
	JoyAxisJointTransformWriter_ProcessMessage_m5B5C492C36074B2FF724622109D171BED8BBFDD0,
	JoyAxisJointTransformWriter_Write_mDF302C82BAF6B5E4DB874DD8808F6710503CEB34,
	JoyAxisJointTransformWriter__ctor_mD42420996750B6B019AC9C8C4E934BC75C6CDD1F,
	JoyAxisReader_Read_mEDF02F4243E9157760679963438D24BDBFE9A10F,
	JoyAxisReader__ctor_mC55AA058504740F49A4FB26FBDB3880ED11F869B,
	NULL,
	JoyAxisWriter__ctor_mC44849F84B490B377D178FB9BBEE63DDBA647B12,
	JoyButtonReader_Read_m054C6A69060C966B3292589E6D7ADC6A34507E88,
	JoyButtonReader__ctor_mE893F35E1D15F8F02E9CBE51DD91CC0E7E12490B,
	NULL,
	JoyButtonWriter__ctor_m64CA3ED90E7691752100974C4007FAEDDCA64B06,
	LaserScanReader_Start_m8B2563F582A63D4DF11F70318B5497FD6A04FD9A,
	LaserScanReader_Scan_m79A4B76F8326E591E124E03EDA51D7E3AFD55FDB,
	LaserScanReader_MeasureDistance_mAB31F7634AC27C2A4A2E9E79F6D22C867B5A8EE4,
	LaserScanReader__ctor_m206FA6C04878C6AD878328D56E63034C2C4A3D27,
	LaserScanWriter_Update_mF4A603A8C05741AC35C2ACB3FE97C76C90B5B83E,
	LaserScanWriter_Write_m9FFDDA606BF7904DACF33D055184D8B9A2FBBE5B,
	LaserScanWriter__ctor_m9FD31A8392EBD0E2BED67C23155619FED7AE0589,
	GetParamServiceProvider_ServiceCallHandler_m3423A16E0FEA7ADC85BF8B8D67906963E5636183,
	GetParamServiceProvider__ctor_m558B2C91B70231B0582423EE69C5673BD5293A50,
	ImagePublisher_Start_m67A3E75572942369FC1E4AC4CF2F079FA7494DF7,
	ImagePublisher_UpdateImage_m2291765CC1D3A91EE5FEFE95E20D1F6B3BD29FEF,
	ImagePublisher_InitializeGameObject_mC474EFE9B8464B7DC53A15AA297CF0EFB1A398EB,
	ImagePublisher_InitializeMessage_m38C65D1F792C7792B1C77B597ECA72CC456ED5DF,
	ImagePublisher_UpdateMessage_mD4BAE26E9F4BDBC2D78FA1F1F070752BEC81B8C5,
	ImagePublisher__ctor_mA2A66F6BEDA545C26292C4954ED3BAEB7886F1E4,
	ImageSubscriber_Start_mD67094ED4CB99E6C320D966B940867DE5EEED08D,
	ImageSubscriber_Update_m47094FAD2142A197D46DC32B9FFB6E1AAC7ADC04,
	ImageSubscriber_ReceiveMessage_m030609D17F23129A6790DEF0BFD54D546EBFDF00,
	ImageSubscriber_ProcessMessage_m10E95382F3FE1BB86BABB0D0C8ADC66CE6A33DCF,
	ImageSubscriber__ctor_mABFDB8D0A2150BB34E55E45BE9B8002AD18563C8,
	JointStatePublisher_Start_m3C4944420983070BE12820D4BE41DD1B011FA70F,
	JointStatePublisher_FixedUpdate_m4A6F030633F1589299ED3AB21F111B8D84ADA0AB,
	JointStatePublisher_InitializeMessage_m6643F8A92D59E61376B17C08F3CB1E1D9C741E54,
	JointStatePublisher_UpdateMessage_m6EC84B84134ADB515FBB71C574DE0B9C90A9D433,
	JointStatePublisher_UpdateJointState_m49D05DCFB8F5B1FA1E67249EDE76A54A0E50AE66,
	JointStatePublisher__ctor_m3A9993A4018A470961AD3E89901178677451E7DB,
	JointStateSubscriber_ReceiveMessage_m086F869D3638D2D8BEC91EF65AE3A5A5E3F9447F,
	JointStateSubscriber__ctor_m362303B05D4AB7C553F6BD720EDDE8AAB3765E3E,
	JoyPublisher_Start_mD20F8147EB1EFE683841C01F78E171DD25097C2B,
	JoyPublisher_Update_mC5104CF10964B2EF879FB83BA61A8AE42CCA4D09,
	JoyPublisher_InitializeGameObject_mFCE117F7916E76FD5FDFD55F743FBFCC2D37E5C7,
	JoyPublisher_InitializeMessage_m595D02A82EB51675111CAFFC5C7B9AEF1C0E20DF,
	JoyPublisher_UpdateMessage_m4C5D6E44A6718D4327727AFE0F8AC414D28C463D,
	JoyPublisher__ctor_m537C3685BB4F780B0DF990DD4C5433B3E548901A,
	JoySubscriber_Start_mCE870A3CD4751CC9E68B898092D917B42424FB80,
	JoySubscriber_ReceiveMessage_m47E2258B644484757FF54A2529810E50DBF1EEC3,
	JoySubscriber__ctor_m6D77066CEA7DF62D5A3F7E6B9DA17DB39C3DEB8F,
	LaserScanPublisher_Start_m366551950DBFB199F46C56D3B67D140A01A6972C,
	LaserScanPublisher_FixedUpdate_m736CF9DB1866A578FBB0B36A8918A9B574A2B266,
	LaserScanPublisher_InitializeMessage_mF79ADEB9614A1DA7F5EF42272305417200BCB56F,
	LaserScanPublisher_UpdateMessage_mD9FDEC6D402BF9504FD1C2A40429DBD8E204CABC,
	LaserScanPublisher__ctor_m853590B41D6C19A8FA91933AF7D7B2C66D871CB0,
	LaserScanSubscriber_Start_mD6EE9F911AAFC6209EB68D938B2E847D77E91AA3,
	LaserScanSubscriber_ReceiveMessage_m899CB9C68C15D9458C093FD2D1AFEBE1ADB77743,
	LaserScanSubscriber__ctor_m1AE15B436B1052FFFAAC553A12999C80B238C5FE,
	OdometrySubscriber_Start_m57B0F4438AE55C2D1919F9993DE41296A031845C,
	OdometrySubscriber_Update_m2DAD3ECF2AB47D6342C734636CA607F71BB0A120,
	OdometrySubscriber_ReceiveMessage_mA1FEE5C5EF20764E610EE015FF971046C91130A6,
	OdometrySubscriber_ProcessMessage_mC69971D7FC944961492E23EE8E6AFE2B1A0A6F9E,
	OdometrySubscriber_GetPosition_mB09EB6668660424D92C1327C60D9CE4BA03BBAD5,
	OdometrySubscriber_GetRotation_m7D23E7E87212642FCD5D7E12B2F4E56A1E3D3F17,
	OdometrySubscriber__ctor_m1A2D7A442AC73E84A8EBE293BA62292698583856,
	PoseStampedPublisher_Start_m306D99490083CF75E9639CFB15AA777ABDB68238,
	PoseStampedPublisher_FixedUpdate_m5802F25366565F526D4682705CE4A99F5BF66F7D,
	PoseStampedPublisher_InitializeMessage_m27D97CF13250BB6029CC1F812C274BAF379CF44D,
	PoseStampedPublisher_UpdateMessage_m17DC255DF0553720F03348E0A13091FBD72AD2DA,
	PoseStampedPublisher_GetGeometryPoint_m5AC7EEA2CAACA619A7BAA32A35FD2667BAAB4509,
	PoseStampedPublisher_GetGeometryQuaternion_m3B31AE1BB5A605397CEF1D2701104BBA1BE62F9C,
	PoseStampedPublisher__ctor_m5CC34F0A93C7C66F3BD057B79EC1E0609D7BD874,
	PoseStampedSubscriber_Start_m85EAB7877C014A82F43CA7CD27D8F71508142EE0,
	PoseStampedSubscriber_Update_mC5C081CB7616AF23D81F7AE6F312BA4CD3B2F6AC,
	PoseStampedSubscriber_ReceiveMessage_m188037AE972B413E9C2318CF05161FE0332973C9,
	PoseStampedSubscriber_ProcessMessage_m7F111F5AC23B48BF22B134181F07EFB435DD7C7E,
	PoseStampedSubscriber_GetPosition_m7C99B357E3D4F249E00F958443DB1A0411C250EB,
	PoseStampedSubscriber_GetRotation_m9235DC8387A86D00CFD1C2BA44BFD267D4E06862,
	PoseStampedSubscriber__ctor_mC9F379080F6154169E7A399E3FF9EE53B806ED14,
	RosConnector_get_RosSocket_m26BD99A690D5BAF9C2FDD19AA34F424814787CCF,
	RosConnector_set_RosSocket_m51E668B3EB3AE10900C3C638CB87EEC62E2DB345,
	RosConnector_get_IsConnected_mD887BDF52CC256253976C87BD83EF0E00F90ED56,
	RosConnector_set_IsConnected_m6C2CF420228F86A69B2FAEAB15BBD7D633942D1C,
	RosConnector_Awake_m11893337639AEDF08AB07F63F6141768F7C002BE,
	RosConnector_ConnectAndWait_m1EC4CF8AF4BF61AA53D97A90B8ADDC56252DB62C,
	RosConnector_ConnectToRos_m73C3ADC10993DFE93B1E4D48CF217D3301D81FEB,
	RosConnector_GetProtocol_m5493BA0951116937B868C37B4810153F95668142,
	RosConnector_OnApplicationQuit_mE3FDF1BCE3FD48B30B95AD8397FDAE31C81BE874,
	RosConnector_OnConnected_m99790D627FCC2E4128DE6B58071F58262641083D,
	RosConnector_OnClosed_m96CC3481B1810ECD8727D6C4B32A4FE7AFE2AF20,
	RosConnector__ctor_m2A6E4432B5754A91BF99FA4EA96B6CDDE18E4EE0,
	TwistPublisher_Start_mC3CB262522036C1BAD8B70664F7FC83C4CED9476,
	TwistPublisher_FixedUpdate_m8CFF85AA29399829F28CB0DA7FD961027C2635DA,
	TwistPublisher_InitializeMessage_m1AE1ED14C80E4E1CDA3D16D56D3FC2DB9F4462A6,
	TwistPublisher_UpdateMessage_m79F4E16AA7A2BD1BADD5FA427EC01A9F1640421E,
	TwistPublisher_GetGeometryVector3_m280A75814E489E78D750A9DFFB84CCC7F615E48C,
	TwistPublisher__ctor_m595A5C6EDBC36987BC4A88194577281AE4C1CAC2,
	TwistSubscriber_Start_mFAE0B0CCD7954FC40F2701FD025A13E9453C55C3,
	TwistSubscriber_ReceiveMessage_mCD49E95A982BEDB39C5C7C20E087911FBBEF89FA,
	TwistSubscriber_ToVector3_mF32BD3534F3B2DA5E37D39592C118951858F632F,
	TwistSubscriber_Update_m41B43FDEE75A35B0E7113E386F9C1D2B4E230D99,
	TwistSubscriber_ProcessMessage_m2CC679AAC66AB38F4E77AC011B1134CD695F0F48,
	TwistSubscriber__ctor_m97903704E8A5BC94B661043FC205334E1F4DC7CF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LaserScanVisualizer_Update_m8BA6863E933DDC31CC226E80A3153AE3AB5718EC,
	LaserScanVisualizer_OnDisable_mE135111F72D59A25F6EC8351F4305AF6DEC60057,
	LaserScanVisualizer_SetSensorData_mAB4FC5A5C0175AABEEF47A03E27C71ECD17CF2A6,
	LaserScanVisualizer_GetColor_mFD8FB9975D4B509FACB3D0D52246981684EBD1D8,
	LaserScanVisualizer__ctor_m8F96510A95D0883091471EE4E3CC9D404975359C,
	LaserScanVisualizerLines_Create_m671E478127AC68844D1D75D89AC633AC7A6012FD,
	LaserScanVisualizerLines_Visualize_m93F0AE35BA03095F7FB9246DEDBFCB9D003A0770,
	LaserScanVisualizerLines_DestroyObjects_m2C041C6F1E565B1AE1E5893EC0F3C1911B933997,
	LaserScanVisualizerLines__ctor_m0B9FF6E18820F2E08B321F56A05D3C14088BEAFA,
	LaserScanVisualizerMesh_Create_m30A0FB87369307FFD324EF2CD997CA8436069509,
	LaserScanVisualizerMesh_Visualize_m1C7A702A745070A7FC29F91920C136CD4DB43A16,
	LaserScanVisualizerMesh_DestroyObjects_m175FBA7D4F1B2AA3DF6326C03FF54113945E0991,
	LaserScanVisualizerMesh__ctor_mD740705D50E55B5B5EBDADBC1C09E29405D5660E,
	LaserScanVisualizerSpheres_Create_m4D18B5A8C30F8458233FB11BF8AB093CD4EEC2B5,
	LaserScanVisualizerSpheres_Visualize_mC1DE9D7817D5C3B57EB2FB2445E7CF92DDCE2A62,
	LaserScanVisualizerSpheres_DestroyObjects_m0E0913B4AA3E040441984A9B23A4B3E8723FD5F1,
	LaserScanVisualizerSpheres__ctor_mAE41CAF64198208F1D10B4AC1F894FEBAC427B02,
	Timer_Now_m5C42BB8E9874B2A27C5CD610895811D2E1403A5C,
	Timer_Now_m436BED554FB77406699EE5D4837E13C859F9FF9E,
	Timer_Now_m95C982DCD42DCF8EBABAF66301E91A6B78DE1E18,
	Timer__ctor_m97466C6A4381C1535FDD8E013DF4376C3983E199,
	Timer__cctor_m2D2910DF419FB0DCFEFD340336AD760750A91CFF,
	UnityFibonacciActionClient_Start_mE516BD32AE216A8768EEB3AB944C55B3B08CC31B,
	UnityFibonacciActionClient_Update_m4E8C9C72A867AF1B50428E40EA62352689AD2319,
	UnityFibonacciActionClient_RegisterGoal_mA88B5024EC5AE582261A1B1334F4AF29BA8E9511,
	UnityFibonacciActionClient__ctor_mC1BDC0B71922FEE7880D5A1364B43EE6C0BAF67F,
	UnityFibonacciActionSever_Start_m93E6FA8FD95397FC447ED989CFECFDB1167432EA,
	UnityFibonacciActionSever_Update_mA57398617B3915BCEAF687E40D0426DCB3B727B0,
	UnityFibonacciActionSever__ctor_m8301330C5AC047490A9B31DB39496E89C395FEFC,
	U3CU3Ec__cctor_mE5D4E1597C71D0C3E04B77271A45E68550CEEB8B,
	U3CU3Ec__ctor_m5B0E1DED4BDFEF400D2768BE929BAEFE44A7B27B,
	U3CU3Ec_U3CStartU3Eb__5_0_m7AE32C9CE21D8F0252797845F7CB6CDAB40F44CB,
};
static const int32_t s_InvokerIndices[356] = 
{
	-1,
	-1,
	7754,
	8541,
	8337,
	9052,
	9052,
	9052,
	9052,
	8834,
	8834,
	8809,
	9048,
	8792,
	6204,
	5175,
	6112,
	5087,
	6112,
	5087,
	6112,
	5087,
	6204,
	5175,
	6204,
	5175,
	6265,
	6265,
	6265,
	6265,
	6265,
	4645,
	4258,
	6265,
	6265,
	7537,
	2930,
	6265,
	6265,
	6265,
	6265,
	6265,
	8185,
	5130,
	6265,
	5175,
	5130,
	5130,
	5130,
	4526,
	2708,
	8119,
	8121,
	8119,
	8121,
	8121,
	8119,
	8262,
	8140,
	8141,
	6204,
	6204,
	6035,
	6155,
	2451,
	6260,
	4532,
	4781,
	2440,
	1571,
	4546,
	3659,
	6265,
	6265,
	8352,
	6265,
	6265,
	6265,
	6265,
	6265,
	5130,
	9052,
	8792,
	9052,
	7604,
	6155,
	8792,
	8794,
	8792,
	8792,
	6265,
	6112,
	5087,
	6035,
	7745,
	8115,
	8347,
	6265,
	6204,
	6204,
	6204,
	5175,
	5175,
	8654,
	5130,
	9048,
	9222,
	8693,
	8694,
	8892,
	6155,
	8792,
	4532,
	6155,
	6035,
	6035,
	6265,
	4546,
	6035,
	6265,
	6265,
	8792,
	6204,
	6204,
	6204,
	5175,
	5130,
	4532,
	6265,
	8792,
	6035,
	6265,
	8792,
	6204,
	6035,
	6265,
	8792,
	6204,
	5130,
	4532,
	6155,
	6035,
	6035,
	6265,
	8792,
	6204,
	5175,
	5130,
	4532,
	6035,
	6155,
	6265,
	8792,
	6204,
	6204,
	6204,
	5175,
	5130,
	4532,
	6035,
	6155,
	6265,
	6265,
	8352,
	9048,
	9048,
	8792,
	8792,
	8792,
	8352,
	6155,
	6265,
	8352,
	6155,
	6265,
	9224,
	6265,
	4532,
	3659,
	5008,
	5008,
	5008,
	5008,
	6265,
	6265,
	6265,
	6265,
	9074,
	9224,
	5008,
	5008,
	6265,
	6265,
	1046,
	6265,
	6265,
	6265,
	6265,
	5175,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	5175,
	6265,
	6265,
	6265,
	6265,
	6265,
	5175,
	6265,
	6204,
	6265,
	5175,
	6265,
	6035,
	6265,
	5087,
	6265,
	6265,
	6155,
	6265,
	6265,
	6265,
	5130,
	6265,
	1954,
	6265,
	6265,
	5130,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	5130,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	5087,
	6265,
	5130,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	5130,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	5130,
	6265,
	6265,
	6265,
	5130,
	6265,
	4781,
	4576,
	6265,
	6265,
	6265,
	6265,
	6265,
	8377,
	8362,
	6265,
	6265,
	6265,
	5130,
	6265,
	4781,
	4576,
	6265,
	6155,
	5130,
	6155,
	5130,
	6265,
	6265,
	6824,
	8101,
	6265,
	2930,
	2930,
	6265,
	6265,
	6265,
	6265,
	6265,
	8809,
	6265,
	6265,
	5130,
	9048,
	6265,
	6265,
	6265,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	6265,
	6265,
	6265,
	6265,
	507,
	3880,
	6265,
	5087,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	5087,
	6265,
	6265,
	6265,
	6155,
	5130,
	8291,
	6265,
	9224,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	6265,
	9224,
	6265,
	5130,
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x02000037, { 5, 2 } },
	{ 0x02000038, { 7, 4 } },
	{ 0x02000039, { 11, 5 } },
	{ 0x06000001, { 0, 2 } },
	{ 0x06000002, { 2, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[16] = 
{
	{ (Il2CppRGCTXDataType)3, 61502 },
	{ (Il2CppRGCTXDataType)2, 599 },
	{ (Il2CppRGCTXDataType)3, 61503 },
	{ (Il2CppRGCTXDataType)2, 600 },
	{ (Il2CppRGCTXDataType)3, 62250 },
	{ (Il2CppRGCTXDataType)3, 63327 },
	{ (Il2CppRGCTXDataType)2, 1115 },
	{ (Il2CppRGCTXDataType)3, 53831 },
	{ (Il2CppRGCTXDataType)2, 13151 },
	{ (Il2CppRGCTXDataType)3, 50411 },
	{ (Il2CppRGCTXDataType)3, 63334 },
	{ (Il2CppRGCTXDataType)3, 53838 },
	{ (Il2CppRGCTXDataType)3, 53837 },
	{ (Il2CppRGCTXDataType)2, 13293 },
	{ (Il2CppRGCTXDataType)3, 50726 },
	{ (Il2CppRGCTXDataType)3, 63350 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	356,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	5,
	s_rgctxIndices,
	16,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
