﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean System.SR::UsingResourceKeys()
extern void SR_UsingResourceKeys_mEF65790170451FCC6479139BA61D045727FB6354 (void);
// 0x00000002 System.String System.SR::GetResourceString(System.String,System.String)
extern void SR_GetResourceString_m575E3F60E0C64951E783D0D5C1781756E1AD1D7D (void);
// 0x00000003 System.Resources.ResourceManager System.SR::get_ResourceManager()
extern void SR_get_ResourceManager_m336BBA2C23A0675483FC6419305949889D08C0A1 (void);
// 0x00000004 System.String System.SR::get_ChannelClosedException_DefaultMessage()
extern void SR_get_ChannelClosedException_DefaultMessage_m132FAF68F1DB428AE2EE9CF60292D2FF0A890551 (void);
// 0x00000005 System.String System.SR::get_InvalidOperation_IncompleteAsyncOperation()
extern void SR_get_InvalidOperation_IncompleteAsyncOperation_mA67B1F99FC8891CD6D8C91D2AFB01B4427F811EB (void);
// 0x00000006 System.String System.SR::get_InvalidOperation_MultipleContinuations()
extern void SR_get_InvalidOperation_MultipleContinuations_m25D1022F3D151623F43B17F682D501826507E018 (void);
// 0x00000007 System.String System.SR::get_InvalidOperation_IncorrectToken()
extern void SR_get_InvalidOperation_IncorrectToken_mDF74EA657CCACC032AFA580F30DF4ACD112248A6 (void);
// 0x00000008 System.Void System.Threading.Channels.AsyncOperation::AvailableSentinel(System.Object)
extern void AsyncOperation_AvailableSentinel_mC72A516C28C7A5AA7CC0B131F4807921E5D4E6E7 (void);
// 0x00000009 System.Void System.Threading.Channels.AsyncOperation::CompletedSentinel(System.Object)
extern void AsyncOperation_CompletedSentinel_mA3A489BFC05872D325FA1545221608CFE7883612 (void);
// 0x0000000A System.Void System.Threading.Channels.AsyncOperation::ThrowIncompleteOperationException()
extern void AsyncOperation_ThrowIncompleteOperationException_mF635D1C0225DE2DACC51C86281CE7539938CADF2 (void);
// 0x0000000B System.Void System.Threading.Channels.AsyncOperation::ThrowMultipleContinuations()
extern void AsyncOperation_ThrowMultipleContinuations_m199393578B986981F642CC6F9BCEB99C4EB309C6 (void);
// 0x0000000C System.Void System.Threading.Channels.AsyncOperation::ThrowIncorrectCurrentIdException()
extern void AsyncOperation_ThrowIncorrectCurrentIdException_m05E3285A6328DF61F82DE29B815403151A0E2EF4 (void);
// 0x0000000D System.Void System.Threading.Channels.AsyncOperation::.ctor()
extern void AsyncOperation__ctor_m4016C622195A1EC78CCDDB5D43EEB866F64696C0 (void);
// 0x0000000E System.Void System.Threading.Channels.AsyncOperation::.cctor()
extern void AsyncOperation__cctor_m2C4F894D3B2ADDD792006315FD7581FE2F766A90 (void);
// 0x0000000F System.Void System.Threading.Channels.AsyncOperation`1::.ctor(System.Boolean,System.Threading.CancellationToken,System.Boolean)
// 0x00000010 System.Threading.Channels.AsyncOperation`1<TResult> System.Threading.Channels.AsyncOperation`1::get_Next()
// 0x00000011 System.Void System.Threading.Channels.AsyncOperation`1::set_Next(System.Threading.Channels.AsyncOperation`1<TResult>)
// 0x00000012 System.Threading.CancellationToken System.Threading.Channels.AsyncOperation`1::get_CancellationToken()
// 0x00000013 System.Threading.Tasks.ValueTask`1<TResult> System.Threading.Channels.AsyncOperation`1::get_ValueTaskOfT()
// 0x00000014 System.Threading.Tasks.Sources.ValueTaskSourceStatus System.Threading.Channels.AsyncOperation`1::GetStatus(System.Int16)
// 0x00000015 System.Boolean System.Threading.Channels.AsyncOperation`1::get_IsCompleted()
// 0x00000016 TResult System.Threading.Channels.AsyncOperation`1::GetResult(System.Int16)
// 0x00000017 System.Boolean System.Threading.Channels.AsyncOperation`1::TryOwnAndReset()
// 0x00000018 System.Void System.Threading.Channels.AsyncOperation`1::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16,System.Threading.Tasks.Sources.ValueTaskSourceOnCompletedFlags)
// 0x00000019 System.Void System.Threading.Channels.AsyncOperation`1::UnregisterCancellation()
// 0x0000001A System.Boolean System.Threading.Channels.AsyncOperation`1::TrySetResult(TResult)
// 0x0000001B System.Boolean System.Threading.Channels.AsyncOperation`1::TrySetException(System.Exception)
// 0x0000001C System.Boolean System.Threading.Channels.AsyncOperation`1::TrySetCanceled(System.Threading.CancellationToken)
// 0x0000001D System.Boolean System.Threading.Channels.AsyncOperation`1::TryReserveCompletionIfCancelable()
// 0x0000001E System.Void System.Threading.Channels.AsyncOperation`1::SignalCompletion()
// 0x0000001F System.Void System.Threading.Channels.AsyncOperation`1::SetCompletionAndInvokeContinuation()
// 0x00000020 System.Void System.Threading.Channels.AsyncOperation`1::UnsafeQueueSetCompletionAndInvokeContinuation()
// 0x00000021 System.Void System.Threading.Channels.AsyncOperation`1::QueueUserWorkItem(System.Action`1<System.Object>,System.Object)
// 0x00000022 System.Threading.CancellationTokenRegistration System.Threading.Channels.AsyncOperation`1::UnsafeRegister(System.Threading.CancellationToken,System.Action`1<System.Object>,System.Object)
// 0x00000023 System.Void System.Threading.Channels.AsyncOperation`1/<>c::.cctor()
// 0x00000024 System.Void System.Threading.Channels.AsyncOperation`1/<>c::.ctor()
// 0x00000025 System.Void System.Threading.Channels.AsyncOperation`1/<>c::<.ctor>b__11_0(System.Object)
// 0x00000026 System.Void System.Threading.Channels.AsyncOperation`1/<>c::<OnCompleted>b__29_0(System.Object)
// 0x00000027 System.Void System.Threading.Channels.AsyncOperation`1/<>c::<SignalCompletion>b__35_0(System.Object)
// 0x00000028 System.Void System.Threading.Channels.AsyncOperation`1/<>c::<SignalCompletion>b__35_1(System.Object)
// 0x00000029 System.Void System.Threading.Channels.AsyncOperation`1/<>c::<SetCompletionAndInvokeContinuation>b__36_0(System.Object)
// 0x0000002A System.Void System.Threading.Channels.AsyncOperation`1/<>c::<UnsafeQueueSetCompletionAndInvokeContinuation>b__37_0(System.Object)
// 0x0000002B System.Threading.Channels.Channel`1<T> System.Threading.Channels.Channel::CreateUnbounded(System.Threading.Channels.UnboundedChannelOptions)
// 0x0000002C System.Void System.Threading.Channels.ChannelClosedException::.ctor()
extern void ChannelClosedException__ctor_m40A201AB352AE657915A4183D80C7C86BA517FB8 (void);
// 0x0000002D System.Void System.Threading.Channels.ChannelClosedException::.ctor(System.Exception)
extern void ChannelClosedException__ctor_mD5E7A150ED3270B619EA9CEBCC966D977FD71E5F (void);
// 0x0000002E System.Void System.Threading.Channels.ChannelOptions::set_SingleWriter(System.Boolean)
extern void ChannelOptions_set_SingleWriter_m9F8C78D2B824D99E4670F0BC001F459C3A40BD0B (void);
// 0x0000002F System.Boolean System.Threading.Channels.ChannelOptions::get_SingleReader()
extern void ChannelOptions_get_SingleReader_mAB7BE8C40DCDA9B6522593700F672636FF14E714 (void);
// 0x00000030 System.Void System.Threading.Channels.ChannelOptions::set_SingleReader(System.Boolean)
extern void ChannelOptions_set_SingleReader_mDEF8BB3DF5398497F729D2E22140D08A8347B501 (void);
// 0x00000031 System.Boolean System.Threading.Channels.ChannelOptions::get_AllowSynchronousContinuations()
extern void ChannelOptions_get_AllowSynchronousContinuations_mB1363F76616B6FFE8D329C0F0BFEE5B145B7A002 (void);
// 0x00000032 System.Void System.Threading.Channels.ChannelOptions::set_AllowSynchronousContinuations(System.Boolean)
extern void ChannelOptions_set_AllowSynchronousContinuations_m2A512F65B4F514D86FE408343AD5EB044A24EE7F (void);
// 0x00000033 System.Void System.Threading.Channels.ChannelOptions::.ctor()
extern void ChannelOptions__ctor_mF20BF30EF0A3A985DBD9C00155CE295444E27083 (void);
// 0x00000034 System.Void System.Threading.Channels.UnboundedChannelOptions::.ctor()
extern void UnboundedChannelOptions__ctor_m9AEB91CD1559405A28C1BE7A1378FA3EE600DB22 (void);
// 0x00000035 System.Boolean System.Threading.Channels.ChannelReader`1::TryRead(T&)
// 0x00000036 System.Threading.Tasks.ValueTask`1<System.Boolean> System.Threading.Channels.ChannelReader`1::WaitToReadAsync(System.Threading.CancellationToken)
// 0x00000037 System.Void System.Threading.Channels.ChannelReader`1::.ctor()
// 0x00000038 System.Void System.Threading.Channels.ChannelUtilities::Complete(System.Threading.Tasks.TaskCompletionSource`1<System.VoidResult>,System.Exception)
extern void ChannelUtilities_Complete_mBE6451E741A0B2BF8BEC273C933B6A17F23A641B (void);
// 0x00000039 System.Threading.Tasks.ValueTask`1<System.Boolean> System.Threading.Channels.ChannelUtilities::QueueWaiter(System.Threading.Channels.AsyncOperation`1<System.Boolean>&,System.Threading.Channels.AsyncOperation`1<System.Boolean>)
extern void ChannelUtilities_QueueWaiter_m56ECE09562A7CAACCE941D260D13E6C4184D90F7 (void);
// 0x0000003A System.Void System.Threading.Channels.ChannelUtilities::WakeUpWaiters(System.Threading.Channels.AsyncOperation`1<System.Boolean>&,System.Boolean,System.Exception)
extern void ChannelUtilities_WakeUpWaiters_m4C113A2BAB644A088B526FA371F942CD829DD4DA (void);
// 0x0000003B System.Void System.Threading.Channels.ChannelUtilities::FailOperations(System.Collections.Generic.Deque`1<T>,System.Exception)
// 0x0000003C System.Exception System.Threading.Channels.ChannelUtilities::CreateInvalidCompletionException(System.Exception)
extern void ChannelUtilities_CreateInvalidCompletionException_m1CB8A95EE011DCD85F46356AE4A539DEC94310A1 (void);
// 0x0000003D System.Void System.Threading.Channels.ChannelUtilities::.cctor()
extern void ChannelUtilities__cctor_mA4492E465F21FEC7936A9E20E5E23AE970446123 (void);
// 0x0000003E System.Boolean System.Threading.Channels.ChannelWriter`1::TryComplete(System.Exception)
// 0x0000003F System.Boolean System.Threading.Channels.ChannelWriter`1::TryWrite(T)
// 0x00000040 System.Void System.Threading.Channels.ChannelWriter`1::Complete(System.Exception)
// 0x00000041 System.Void System.Threading.Channels.ChannelWriter`1::.ctor()
// 0x00000042 System.Void System.Threading.Channels.Channel`1::.ctor()
// 0x00000043 System.Threading.Channels.ChannelReader`1<TRead> System.Threading.Channels.Channel`2::get_Reader()
// 0x00000044 System.Void System.Threading.Channels.Channel`2::set_Reader(System.Threading.Channels.ChannelReader`1<TRead>)
// 0x00000045 System.Threading.Channels.ChannelWriter`1<TWrite> System.Threading.Channels.Channel`2::get_Writer()
// 0x00000046 System.Void System.Threading.Channels.Channel`2::set_Writer(System.Threading.Channels.ChannelWriter`1<TWrite>)
// 0x00000047 System.Void System.Threading.Channels.Channel`2::.ctor()
// 0x00000048 System.Void System.Threading.Channels.SingleConsumerUnboundedChannel`1::.ctor(System.Boolean)
// 0x00000049 System.Object System.Threading.Channels.SingleConsumerUnboundedChannel`1::get_SyncObj()
// 0x0000004A System.Void System.Threading.Channels.SingleConsumerUnboundedChannel`1/UnboundedChannelReader::.ctor(System.Threading.Channels.SingleConsumerUnboundedChannel`1<T>)
// 0x0000004B System.Boolean System.Threading.Channels.SingleConsumerUnboundedChannel`1/UnboundedChannelReader::TryRead(T&)
// 0x0000004C System.Threading.Tasks.ValueTask`1<System.Boolean> System.Threading.Channels.SingleConsumerUnboundedChannel`1/UnboundedChannelReader::WaitToReadAsync(System.Threading.CancellationToken)
// 0x0000004D System.Void System.Threading.Channels.SingleConsumerUnboundedChannel`1/UnboundedChannelWriter::.ctor(System.Threading.Channels.SingleConsumerUnboundedChannel`1<T>)
// 0x0000004E System.Boolean System.Threading.Channels.SingleConsumerUnboundedChannel`1/UnboundedChannelWriter::TryComplete(System.Exception)
// 0x0000004F System.Boolean System.Threading.Channels.SingleConsumerUnboundedChannel`1/UnboundedChannelWriter::TryWrite(T)
// 0x00000050 System.Void System.Threading.Channels.UnboundedChannel`1::.ctor(System.Boolean)
// 0x00000051 System.Object System.Threading.Channels.UnboundedChannel`1::get_SyncObj()
// 0x00000052 System.Void System.Threading.Channels.UnboundedChannel`1/UnboundedChannelReader::.ctor(System.Threading.Channels.UnboundedChannel`1<T>)
// 0x00000053 System.Boolean System.Threading.Channels.UnboundedChannel`1/UnboundedChannelReader::TryRead(T&)
// 0x00000054 System.Void System.Threading.Channels.UnboundedChannel`1/UnboundedChannelReader::CompleteIfDone(System.Threading.Channels.UnboundedChannel`1<T>)
// 0x00000055 System.Threading.Tasks.ValueTask`1<System.Boolean> System.Threading.Channels.UnboundedChannel`1/UnboundedChannelReader::WaitToReadAsync(System.Threading.CancellationToken)
// 0x00000056 System.Void System.Threading.Channels.UnboundedChannel`1/UnboundedChannelWriter::.ctor(System.Threading.Channels.UnboundedChannel`1<T>)
// 0x00000057 System.Boolean System.Threading.Channels.UnboundedChannel`1/UnboundedChannelWriter::TryComplete(System.Exception)
// 0x00000058 System.Boolean System.Threading.Channels.UnboundedChannel`1/UnboundedChannelWriter::TryWrite(T)
// 0x00000059 System.Void System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1::.ctor()
// 0x0000005A System.Void System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1::Enqueue(T)
// 0x0000005B System.Void System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1::EnqueueSlow(T,System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1/Segment<T>&)
// 0x0000005C System.Boolean System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1::TryDequeue(T&)
// 0x0000005D System.Boolean System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1::TryDequeueSlow(System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1/Segment<T>&,T[]&,T&)
// 0x0000005E System.Boolean System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1::get_IsEmpty()
// 0x0000005F System.Collections.Generic.IEnumerator`1<T> System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1::GetEnumerator()
// 0x00000060 System.Collections.IEnumerator System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000061 System.Void System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1/Segment::.ctor(System.Int32)
// 0x00000062 System.Void System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1/<GetEnumerator>d__11::.ctor(System.Int32)
// 0x00000063 System.Void System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1/<GetEnumerator>d__11::System.IDisposable.Dispose()
// 0x00000064 System.Boolean System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1/<GetEnumerator>d__11::MoveNext()
// 0x00000065 T System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1/<GetEnumerator>d__11::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000066 System.Void System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1/<GetEnumerator>d__11::System.Collections.IEnumerator.Reset()
// 0x00000067 System.Object System.Collections.Concurrent.SingleProducerSingleConsumerQueue`1/<GetEnumerator>d__11::System.Collections.IEnumerator.get_Current()
// 0x00000068 System.Boolean System.Collections.Generic.Deque`1::get_IsEmpty()
// 0x00000069 T System.Collections.Generic.Deque`1::DequeueHead()
// 0x0000006A System.Void System.Collections.Generic.Deque`1::.ctor()
static Il2CppMethodPointer s_methodPointers[106] = 
{
	SR_UsingResourceKeys_mEF65790170451FCC6479139BA61D045727FB6354,
	SR_GetResourceString_m575E3F60E0C64951E783D0D5C1781756E1AD1D7D,
	SR_get_ResourceManager_m336BBA2C23A0675483FC6419305949889D08C0A1,
	SR_get_ChannelClosedException_DefaultMessage_m132FAF68F1DB428AE2EE9CF60292D2FF0A890551,
	SR_get_InvalidOperation_IncompleteAsyncOperation_mA67B1F99FC8891CD6D8C91D2AFB01B4427F811EB,
	SR_get_InvalidOperation_MultipleContinuations_m25D1022F3D151623F43B17F682D501826507E018,
	SR_get_InvalidOperation_IncorrectToken_mDF74EA657CCACC032AFA580F30DF4ACD112248A6,
	AsyncOperation_AvailableSentinel_mC72A516C28C7A5AA7CC0B131F4807921E5D4E6E7,
	AsyncOperation_CompletedSentinel_mA3A489BFC05872D325FA1545221608CFE7883612,
	AsyncOperation_ThrowIncompleteOperationException_mF635D1C0225DE2DACC51C86281CE7539938CADF2,
	AsyncOperation_ThrowMultipleContinuations_m199393578B986981F642CC6F9BCEB99C4EB309C6,
	AsyncOperation_ThrowIncorrectCurrentIdException_m05E3285A6328DF61F82DE29B815403151A0E2EF4,
	AsyncOperation__ctor_m4016C622195A1EC78CCDDB5D43EEB866F64696C0,
	AsyncOperation__cctor_m2C4F894D3B2ADDD792006315FD7581FE2F766A90,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ChannelClosedException__ctor_m40A201AB352AE657915A4183D80C7C86BA517FB8,
	ChannelClosedException__ctor_mD5E7A150ED3270B619EA9CEBCC966D977FD71E5F,
	ChannelOptions_set_SingleWriter_m9F8C78D2B824D99E4670F0BC001F459C3A40BD0B,
	ChannelOptions_get_SingleReader_mAB7BE8C40DCDA9B6522593700F672636FF14E714,
	ChannelOptions_set_SingleReader_mDEF8BB3DF5398497F729D2E22140D08A8347B501,
	ChannelOptions_get_AllowSynchronousContinuations_mB1363F76616B6FFE8D329C0F0BFEE5B145B7A002,
	ChannelOptions_set_AllowSynchronousContinuations_m2A512F65B4F514D86FE408343AD5EB044A24EE7F,
	ChannelOptions__ctor_mF20BF30EF0A3A985DBD9C00155CE295444E27083,
	UnboundedChannelOptions__ctor_m9AEB91CD1559405A28C1BE7A1378FA3EE600DB22,
	NULL,
	NULL,
	NULL,
	ChannelUtilities_Complete_mBE6451E741A0B2BF8BEC273C933B6A17F23A641B,
	ChannelUtilities_QueueWaiter_m56ECE09562A7CAACCE941D260D13E6C4184D90F7,
	ChannelUtilities_WakeUpWaiters_m4C113A2BAB644A088B526FA371F942CD829DD4DA,
	NULL,
	ChannelUtilities_CreateInvalidCompletionException_m1CB8A95EE011DCD85F46356AE4A539DEC94310A1,
	ChannelUtilities__cctor_mA4492E465F21FEC7936A9E20E5E23AE970446123,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[106] = 
{
	9157,
	8119,
	9189,
	9189,
	9189,
	9189,
	9189,
	9074,
	9074,
	9224,
	9224,
	9224,
	6265,
	9224,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	6265,
	5130,
	5008,
	6035,
	5008,
	6035,
	5008,
	6265,
	6265,
	-1,
	-1,
	-1,
	8352,
	7801,
	7676,
	-1,
	8792,
	9224,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[16] = 
{
	{ 0x02000007, { 0, 19 } },
	{ 0x02000008, { 19, 7 } },
	{ 0x0200000F, { 34, 1 } },
	{ 0x02000010, { 35, 2 } },
	{ 0x02000013, { 37, 11 } },
	{ 0x02000014, { 48, 7 } },
	{ 0x02000015, { 55, 7 } },
	{ 0x02000016, { 62, 13 } },
	{ 0x02000017, { 75, 8 } },
	{ 0x02000018, { 83, 9 } },
	{ 0x02000019, { 92, 10 } },
	{ 0x0200001A, { 102, 1 } },
	{ 0x0200001D, { 103, 1 } },
	{ 0x0200001E, { 104, 1 } },
	{ 0x0600002B, { 26, 4 } },
	{ 0x0600003B, { 30, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[105] = 
{
	{ (Il2CppRGCTXDataType)2, 1928 },
	{ (Il2CppRGCTXDataType)3, 2 },
	{ (Il2CppRGCTXDataType)3, 3674 },
	{ (Il2CppRGCTXDataType)2, 2462 },
	{ (Il2CppRGCTXDataType)2, 14198 },
	{ (Il2CppRGCTXDataType)3, 55147 },
	{ (Il2CppRGCTXDataType)3, 3676 },
	{ (Il2CppRGCTXDataType)3, 3668 },
	{ (Il2CppRGCTXDataType)3, 3 },
	{ (Il2CppRGCTXDataType)3, 3672 },
	{ (Il2CppRGCTXDataType)3, 3671 },
	{ (Il2CppRGCTXDataType)3, 3670 },
	{ (Il2CppRGCTXDataType)3, 3675 },
	{ (Il2CppRGCTXDataType)3, 3673 },
	{ (Il2CppRGCTXDataType)3, 5 },
	{ (Il2CppRGCTXDataType)3, 6 },
	{ (Il2CppRGCTXDataType)3, 3669 },
	{ (Il2CppRGCTXDataType)3, 4 },
	{ (Il2CppRGCTXDataType)3, 7 },
	{ (Il2CppRGCTXDataType)2, 1938 },
	{ (Il2CppRGCTXDataType)3, 18 },
	{ (Il2CppRGCTXDataType)2, 1938 },
	{ (Il2CppRGCTXDataType)2, 2466 },
	{ (Il2CppRGCTXDataType)3, 3679 },
	{ (Il2CppRGCTXDataType)3, 3678 },
	{ (Il2CppRGCTXDataType)3, 3677 },
	{ (Il2CppRGCTXDataType)2, 13749 },
	{ (Il2CppRGCTXDataType)3, 53569 },
	{ (Il2CppRGCTXDataType)2, 13183 },
	{ (Il2CppRGCTXDataType)3, 50497 },
	{ (Il2CppRGCTXDataType)3, 8011 },
	{ (Il2CppRGCTXDataType)2, 687 },
	{ (Il2CppRGCTXDataType)3, 3685 },
	{ (Il2CppRGCTXDataType)3, 8012 },
	{ (Il2CppRGCTXDataType)3, 4906 },
	{ (Il2CppRGCTXDataType)3, 4920 },
	{ (Il2CppRGCTXDataType)2, 2657 },
	{ (Il2CppRGCTXDataType)2, 13188 },
	{ (Il2CppRGCTXDataType)3, 50504 },
	{ (Il2CppRGCTXDataType)3, 4916 },
	{ (Il2CppRGCTXDataType)2, 2655 },
	{ (Il2CppRGCTXDataType)2, 2658 },
	{ (Il2CppRGCTXDataType)2, 13745 },
	{ (Il2CppRGCTXDataType)3, 53538 },
	{ (Il2CppRGCTXDataType)3, 4921 },
	{ (Il2CppRGCTXDataType)2, 13747 },
	{ (Il2CppRGCTXDataType)3, 53555 },
	{ (Il2CppRGCTXDataType)3, 4922 },
	{ (Il2CppRGCTXDataType)3, 4900 },
	{ (Il2CppRGCTXDataType)2, 2644 },
	{ (Il2CppRGCTXDataType)2, 2467 },
	{ (Il2CppRGCTXDataType)3, 3680 },
	{ (Il2CppRGCTXDataType)3, 50510 },
	{ (Il2CppRGCTXDataType)3, 50511 },
	{ (Il2CppRGCTXDataType)3, 50498 },
	{ (Il2CppRGCTXDataType)3, 4907 },
	{ (Il2CppRGCTXDataType)2, 2651 },
	{ (Il2CppRGCTXDataType)3, 50499 },
	{ (Il2CppRGCTXDataType)3, 50513 },
	{ (Il2CppRGCTXDataType)3, 3681 },
	{ (Il2CppRGCTXDataType)3, 50512 },
	{ (Il2CppRGCTXDataType)3, 3682 },
	{ (Il2CppRGCTXDataType)2, 2760 },
	{ (Il2CppRGCTXDataType)3, 7097 },
	{ (Il2CppRGCTXDataType)2, 3099 },
	{ (Il2CppRGCTXDataType)3, 8013 },
	{ (Il2CppRGCTXDataType)3, 4917 },
	{ (Il2CppRGCTXDataType)2, 2656 },
	{ (Il2CppRGCTXDataType)2, 2659 },
	{ (Il2CppRGCTXDataType)2, 13746 },
	{ (Il2CppRGCTXDataType)3, 53539 },
	{ (Il2CppRGCTXDataType)3, 4923 },
	{ (Il2CppRGCTXDataType)2, 13748 },
	{ (Il2CppRGCTXDataType)3, 53556 },
	{ (Il2CppRGCTXDataType)3, 4924 },
	{ (Il2CppRGCTXDataType)3, 4901 },
	{ (Il2CppRGCTXDataType)2, 2645 },
	{ (Il2CppRGCTXDataType)2, 2469 },
	{ (Il2CppRGCTXDataType)3, 3683 },
	{ (Il2CppRGCTXDataType)3, 7099 },
	{ (Il2CppRGCTXDataType)3, 53540 },
	{ (Il2CppRGCTXDataType)3, 7100 },
	{ (Il2CppRGCTXDataType)3, 53570 },
	{ (Il2CppRGCTXDataType)3, 4908 },
	{ (Il2CppRGCTXDataType)2, 2652 },
	{ (Il2CppRGCTXDataType)3, 53571 },
	{ (Il2CppRGCTXDataType)3, 7102 },
	{ (Il2CppRGCTXDataType)3, 61440 },
	{ (Il2CppRGCTXDataType)3, 8015 },
	{ (Il2CppRGCTXDataType)3, 7101 },
	{ (Il2CppRGCTXDataType)3, 8014 },
	{ (Il2CppRGCTXDataType)3, 3684 },
	{ (Il2CppRGCTXDataType)2, 13132 },
	{ (Il2CppRGCTXDataType)3, 50369 },
	{ (Il2CppRGCTXDataType)3, 50506 },
	{ (Il2CppRGCTXDataType)3, 50505 },
	{ (Il2CppRGCTXDataType)3, 63986 },
	{ (Il2CppRGCTXDataType)3, 50509 },
	{ (Il2CppRGCTXDataType)3, 50508 },
	{ (Il2CppRGCTXDataType)2, 2052 },
	{ (Il2CppRGCTXDataType)3, 600 },
	{ (Il2CppRGCTXDataType)3, 50507 },
	{ (Il2CppRGCTXDataType)2, 15337 },
	{ (Il2CppRGCTXDataType)2, 1289 },
	{ (Il2CppRGCTXDataType)3, 56027 },
};
extern const CustomAttributesCacheGenerator g_System_Threading_Channels_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Threading_Channels_CodeGenModule;
const Il2CppCodeGenModule g_System_Threading_Channels_CodeGenModule = 
{
	"System.Threading.Channels.dll",
	106,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	16,
	s_rgctxIndices,
	105,
	s_rgctxValues,
	NULL,
	g_System_Threading_Channels_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
